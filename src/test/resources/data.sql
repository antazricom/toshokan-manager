DELETE
FROM public.record;
DELETE
FROM public.book_progress;
DELETE
FROM public.book_author;
DELETE
FROM public.book;
DELETE
FROM public.vinyl;
DELETE
FROM public.author;
DELETE
FROM public.artist;
DELETE
FROM public.item;
DELETE
FROM public.category;
DELETE
FROM public.login;

INSERT INTO public.item
(id, created_at)
VALUES (1, CURRENT_TIMESTAMP),
       (2, CURRENT_TIMESTAMP),
       (3, CURRENT_TIMESTAMP),
       (4, CURRENT_TIMESTAMP),
       (5, CURRENT_TIMESTAMP),
       (6, CURRENT_TIMESTAMP),
       (7, CURRENT_TIMESTAMP),
       (8, CURRENT_TIMESTAMP),
       (9, CURRENT_TIMESTAMP),
       (10, CURRENT_TIMESTAMP),
       (11, CURRENT_TIMESTAMP),
       (12, CURRENT_TIMESTAMP),
       (13, CURRENT_TIMESTAMP),
       (14, CURRENT_TIMESTAMP);

ALTER SEQUENCE item_id_seq RESTART WITH 15;

INSERT INTO public.category
(id, name, type, created_at)
VALUES (1, 'Littérature japonaise', 'book', CURRENT_TIMESTAMP),
       (2, 'Littérature française', 'book', CURRENT_TIMESTAMP),
       (3, 'Littérature russe', 'book', CURRENT_TIMESTAMP),
       (4, 'Littérature américaine', 'book', CURRENT_TIMESTAMP),
       (5, 'Fantastique - Fantasy', 'book', CURRENT_TIMESTAMP),
       (6, 'Rap', 'vinyl', CURRENT_TIMESTAMP),
       (7, 'Pop - Rock', 'vinyl', CURRENT_TIMESTAMP),
       (8, 'Electro', 'vinyl', CURRENT_TIMESTAMP),
       (9, 'Metal', 'vinyl', CURRENT_TIMESTAMP);

ALTER SEQUENCE category_id_seq RESTART WITH 10;

INSERT INTO public.author
(id, firstname, lastname, created_at)
VALUES (1, 'Haruki', 'Murakami', CURRENT_TIMESTAMP),
       (2, 'Risa', 'Wataya', CURRENT_TIMESTAMP),
       (3, 'Ernest', 'Hemingway', CURRENT_TIMESTAMP),
       (4, 'Leon', 'Tolstoï', CURRENT_TIMESTAMP),
       (5, 'John Ronald Reuel', 'Tolkien', CURRENT_TIMESTAMP);

ALTER SEQUENCE author_id_seq RESTART WITH 6;

INSERT INTO public.book
(id, isbn, publication_year, title, category_id, nb_pages, lang, edition)
VALUES (1, '2714447074', '2009', '1Q84 Tome 1', 1, 552, 'FR', 'Jean Michel'),
       (2, '2714449840', '2009', '1Q84 Tome 2', 1, 504, 'FR', 'Jean Michel'),
       (3, '2714449859', '2010', '1Q84 Tome 3', 1, 624, 'FR', 'Jean Michel'),
       (4, '2253049417', '1937', 'Le Hobbit', 5, 408, 'FR', 'Jean Michel'),
       (5, '2266154117', '1954', 'Le Seigneur des Anneaux, Tome 1 : La Communauté de l''Anneau', 5, 568, 'FR', 'Jean Michel'),
       (6, '2070364550', '1940', 'Pour qui sonne le glas', 4, 512, 'FR', 'Jean Michel'),
       (7, '207036027X', '1929', 'L''adieu aux armes', 4, 320, 'FR', 'Jean Michel'),
       (8, '2809700168', '2008', 'Appel du pied', 1, 141, 'FR', 'Jean Michel'),
       (9, 'B074X5JZFJ', '2017', 'Guerre et Paix: Intégral', 3, 604, 'FR', 'Jean Michel');
INSERT INTO public.book_author
(author_id, book_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (5, 4),
       (5, 5),
       (3, 6),
       (3, 7),
       (2, 8),
       (4, 9);

INSERT INTO public.book_progress
(id, nb_read_pages, status, created_at, book_id)
VALUES (1, 552, 'COMPLETE', CURRENT_TIMESTAMP, 1),
       (2, 504, 'COMPLETE', CURRENT_TIMESTAMP, 2),
       (3, 624, 'COMPLETE', CURRENT_TIMESTAMP, 3),
       (4, 0, 'NEW', CURRENT_TIMESTAMP, 4),
       (5, 128, 'PAUSED', CURRENT_TIMESTAMP, 5),
       (6, 0, 'NEW', CURRENT_TIMESTAMP, 6),
       (7, 0, 'NEW', CURRENT_TIMESTAMP, 7),
       (8, 141, 'COMPLETE', CURRENT_TIMESTAMP, 8),
       (9, 302, 'READING', CURRENT_TIMESTAMP, 9);

ALTER SEQUENCE book_progress_id_seq RESTART WITH 10;

INSERT INTO public.artist
(id, name, created_at)
VALUES (1, 'U2', CURRENT_TIMESTAMP),
       (2, 'IAM', CURRENT_TIMESTAMP),
       (3, 'John Mayer', CURRENT_TIMESTAMP),
       (4, 'Deftones', CURRENT_TIMESTAMP),
       (5, 'Linkin Park', CURRENT_TIMESTAMP);

ALTER SEQUENCE artist_id_seq RESTART WITH 6;

INSERT INTO public.vinyl
(id, title, publication_year, artist_id, category_id)
VALUES (10, 'The Joshua Tree', '2017', 1, 7),
       (11, 'Minutes to Midnight', '2007', 5, 7),
       (12, 'Koi No Yokan', '2008', 4, 9),
       (13, 'Born and Raised', '2011', 3, 7),
       (14, 'Yasuke', '2017', 2, 6);

INSERT INTO public.record
(id, date, item_id, type, number, location, expense, income)
VALUES (1, CURRENT_TIMESTAMP, 1, 'BOOK', 1, 'Fnac', 15.0, 0),
       (2, CURRENT_TIMESTAMP, 2, 'BOOK', 1, 'Fnac', 15.0, 0),
       (3, CURRENT_TIMESTAMP, 3, 'BOOK', 1, 'Fnac', 15.0, 0),
       (4, CURRENT_TIMESTAMP, 4, 'BOOK', 1, 'Fnac', 15.0, 0),
       (5, CURRENT_TIMESTAMP, 10, 'VINYL', 1, 'Fnac', 30.0, 0),
       (6, CURRENT_TIMESTAMP, 11, 'VINYL', 1, 'Fnac', 30.0, 0),
       (7, CURRENT_TIMESTAMP, 12, 'VINYL', 1, 'Fnac', 30.0, 0),
       (8, CURRENT_TIMESTAMP, 13, 'VINYL', 1, 'Fnac', 30.0, 0);

ALTER SEQUENCE record_id_seq RESTART WITH 9;

INSERT INTO public.login
(id, email, password, role)
VALUES (1, 'root@test.com', '$2y$10$w2dWQl.842t/kOe1X2a8oe8chmrBIukYE8qmySgqDjpLNs9eGlXXa', 'ADMIN');

ALTER SEQUENCE login_id_seq RESTART WITH 2;