package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.CategoryDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.CategoryService;
import com.antazri.toshokan.manager.technical.annotations.test.IntegrationSpringBootTest;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@IntegrationSpringBootTest
public class CategoryServiceImplIntegrationTest {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenAddingNewEntity_shouldReturnAddedEntityWithId() throws DecodingException {
        // Given
        CategoryDto categoryDto = new CategoryDto(RandomString.make(4), RandomString.make(4));

        // When
        CategoryDto addedCategory = this.categoryService.create(categoryDto);

        // Then
        assertNotNull(addedCategory.getId());
    }

    @Test
    void whenUpdatingExistingEntity_shouldReturnupdatedObject() throws DecodingException {
        // Given
        String name = "UPDATEDCATEGORY";
        CategoryDto categoryDto = this.categoryService.getDetails(1).orElseThrow();
        categoryDto.setName(name);

        // When
        CategoryDto updatedCategory = this.categoryService.update(categoryDto);

        // Then
        assertEquals(name, updatedCategory.getName());
    }

    @Test
    void whenRemovingEntity_shouldReturnEmptyOptional() throws DecodingException {
        // When
        CategoryDto newCategory = this.categoryService.create(new CategoryDto(RandomString.make(4), RandomString.make(4)));

        // Then
        assertDoesNotThrow(() -> this.categoryService.remove(IdEncoder.decode(newCategory.getId())));
        assertTrue(this.categoryService.getDetails(IdEncoder.decode(newCategory.getId())).isEmpty());
    }

    @Test
    void whenGettingAllInstances_shouldReturnNonEmptyList() {
        // When
        List<CategoryDto> categories = this.categoryService.getAll();

        // Then
        assertFalse(categories.isEmpty());
    }
}
