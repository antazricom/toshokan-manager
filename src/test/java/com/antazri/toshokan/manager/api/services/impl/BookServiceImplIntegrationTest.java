package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.dto.BookDetailsDto;
import com.antazri.toshokan.manager.api.dto.BookDto;
import com.antazri.toshokan.manager.api.dto.CategoryDto;
import com.antazri.toshokan.manager.api.dto.mapper.BookDetailsMapper;
import com.antazri.toshokan.manager.api.dto.mapper.BookMapper;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.BookService;
import com.antazri.toshokan.manager.model.entity.Book;
import com.antazri.toshokan.manager.technical.annotations.test.IntegrationSpringBootTest;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import net.bytebuddy.utility.RandomString;
import org.bson.assertions.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.time.Year;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@IntegrationSpringBootTest
public class BookServiceImplIntegrationTest {

    @Autowired
    private BookService bookService;

    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenAddingNewBookEntity_shouldReturnWithId() throws DecodingException {
        // Given
        BookDto bookDto = new BookDto(
                RandomString.make(4),
                List.of(new AuthorDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4), RandomString.make(4))),
                Year.now().getValue(),
                "978-2-1234-5678-9",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(
                        IdEncoder.encode(1),
                        RandomString.make(4),
                        RandomString.make(4)
                )
        );

        // When
        BookDto addedBook = this.bookService.create(bookDto);

        // Then
        Assertions.assertNotNull(addedBook.getId());
    }

    @Test
    void whenUpdatingExistingEntity_shouldReturnUpdatedBook() throws DecodingException {
        // Given
        String newtitle = "NEWTITLE";
        BookDetailsDto bookDto = this.bookService.getDetails(2).orElseThrow();
        Book book = BookDetailsMapper.asEntity(bookDto);
        bookDto.setTitle(newtitle);

        // When
        BookDto updatedBook = this.bookService.update(BookMapper.asDto(book));

        // Then
        assertEquals(newtitle, updatedBook.getTitle());
    }

    @Test
    void whenRemovingEntity_shouldReturnEmptyOptional() throws DecodingException {
        // When
        BookDto bookDto = new BookDto(
                RandomString.make(4),
                List.of(new AuthorDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4), RandomString.make(4))),
                Year.now().getValue(),
                "978-2-1234-5678-9",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(
                        IdEncoder.encode(1),
                        RandomString.make(4),
                        RandomString.make(4)
                )
        );
        BookDto newBook = this.bookService.create(bookDto);

        // Then
        assertDoesNotThrow(() -> this.bookService.remove(IdEncoder.decode(newBook.getId())));
        assertTrue(this.bookService.getDetails(IdEncoder.decode(newBook.getId())).isEmpty());
    }

    @Test
    void whenGettingAllInstances_shouldReturnNonEmptyList() {
        // When
        List<BookDto> books = this.bookService.getAll();

        // Then
        assertFalse(books.isEmpty());
    }
}
