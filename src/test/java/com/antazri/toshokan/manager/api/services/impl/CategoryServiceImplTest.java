package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.CategoryDto;
import com.antazri.toshokan.manager.api.services.CategoryService;
import com.antazri.toshokan.manager.data.repositories.CategoryDao;
import com.antazri.toshokan.manager.model.entity.Category;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class CategoryServiceImplTest {

    private final CategoryDao categoryDao = mock(CategoryDao.class);
    private final CategoryService categoryService = new CategoryServiceImpl(categoryDao);

    @Test
    void whenGettingDetailsWithNotFoundId_shouldReturnEmptyOptional() {
        // Given
        when(categoryDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<CategoryDto> result = categoryService.getDetails(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingDetails_shouldOptionalDto() {
        // Given
        Category category = new Category(2, "Name");

        when(categoryDao.findById(anyInt())).thenReturn(Optional.of(category));

        // When
        Optional<CategoryDto> result = categoryService.getDetails(2);

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertFalse(result.get().getId().isBlank());
            assertEquals("Name", result.get().getName());
            assertEquals(CategoryDto.class, result.get().getClass());
        });
    }

    @Test
    void whenGettingAllInstances_shouldReturnAsAListOfDto() {
        // Given
        List<Category> categories = new ArrayList<>();
        categories.add(new Category(2, "Name"));

        when(categoryDao.findAll()).thenReturn(categories);

        // When
        List<CategoryDto> result = categoryService.getAll();

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertEquals("Name", result.get(0).getName());
            assertEquals(CategoryDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenAddingNotValidCategory_shouldThrowException() {
        // Then
        assertThrows(IllegalArgumentException.class, () -> categoryService.create(new CategoryDto(" ", "book")));
    }

    @Test
    void whenUpdatingNotValidCategory_shouldThrowException() {
        // Then
        assertThrows(IllegalArgumentException.class, () -> categoryService.create(new CategoryDto("ODhbr1M9x88SeX9XguynOkU", " ", "book")));
    }

    @Test
    void whenTryingTORemoveAnUnknownCategory_shouldthrowException() {
        // When
        when(categoryDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IllegalArgumentException.class, () -> categoryService.remove(1));
    }
}