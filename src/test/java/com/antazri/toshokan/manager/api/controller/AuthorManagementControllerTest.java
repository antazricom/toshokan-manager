package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.technical.annotations.test.ControllerSpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled("Waiting for SpringSecurity update")
@ControllerSpringBootTest
class AuthorManagementControllerTest {

    private static final Class<AuthorDto> TYPE = AuthorDto.class;
    private static final String BASE_PATH = "/authors";
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenSendingRequestWithoutCredentials_shouldReturnHttp403() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenRequestingInstanceDetailsWithValidId_shouldReturnHttp200() throws Exception {
        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH + "/{id}", IdEncoder.encode(1))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        AuthorDto authorDto = objectMapper.readValue(result.getResponse().getContentAsString(), TYPE);

        assertEquals("Murakami", authorDto.getLastname());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenRequestingArtistdetailsWithNotValidId_shouldReturnHttp404() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH + "{id}", IdEncoder.encode(1000))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenSendingARequestForAllInstances_shouldReturnHttp200() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenCreatingNewInstance_shouldReturnHttp201() throws Exception {
        AuthorDto requestBody = new AuthorDto(RandomString.make(8), null, RandomString.make(8));

        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(requestBody))
                ).andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenUpdatingInstanceWithValidId_shouldReturnHttp200() throws Exception {
        AuthorDto update = new AuthorDto(IdEncoder.encode(2), "FIRSTNAME", "MIDDLENAME", "LASTNAME");
        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .put(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(update))
                ).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        AuthorDto authorDto = objectMapper.readValue(result.getResponse().getContentAsString(), TYPE);

        assertEquals("LASTNAME", authorDto.getLastname());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenUpdatingInstanceWithNotValidId_shouldReturnHttp400() throws Exception {
        AuthorDto update = new AuthorDto(IdEncoder.encode(1000), RandomString.make(8), "", RandomString.make(8));
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .put(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(update))
                ).andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenRemovingInstance_shouldReturnHttp200() throws Exception {
        AuthorDto req = new AuthorDto(RandomString.make(8), "", RandomString.make(8));
        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(req))
                ).andExpect(status().isCreated())
                .andReturn();

        AuthorDto body = objectMapper.readValue(result.getResponse().getContentAsString(), TYPE);

        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(BASE_PATH + "/{id}", body.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk());
    }
}