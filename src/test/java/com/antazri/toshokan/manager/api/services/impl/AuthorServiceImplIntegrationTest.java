package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.AuthorService;
import com.antazri.toshokan.manager.technical.annotations.test.IntegrationSpringBootTest;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@IntegrationSpringBootTest
public class AuthorServiceImplIntegrationTest {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenAddingNewAuthorEntity_shouldReturnAsDtoWithId() throws DecodingException {
        // Given
        AuthorDto authorDto = new AuthorDto(RandomString.make(4), RandomString.make(4), RandomString.make(4));

        // When
        AuthorDto a = this.authorService.create(authorDto);

        // Then
        Assertions.assertNotNull(a.getId());
    }

    @Test
    void whenUpdatingExistingAuthorEntity_shouldReturnUpdatedObject() throws DecodingException {
        // Given
        String middlename = "LASTNAMECHANGED";
        AuthorDto authorDto = this.authorService.getDetails(2).orElseThrow();
        authorDto.setMiddlename(middlename);

        // When
        AuthorDto updatedAuthor = this.authorService.update(authorDto);

        // Then
        assertEquals(middlename, updatedAuthor.getMiddlename());
    }

    @Test
    void whenRemovingArtist_shouldReturnEmptyOptionalFromDetailsEndpoint() throws DecodingException {
        // Given
        AuthorDto testAuthor = new AuthorDto(RandomString.make(4), RandomString.make(4), RandomString.make(4));

        // When
        AuthorDto addedTestAuthor = this.authorService.create(testAuthor);
        int addedTestAuthorId = IdEncoder.decode(addedTestAuthor.getId());

        // Then
        assertDoesNotThrow(() -> this.authorService.remove(addedTestAuthorId));
        assertTrue(this.authorService.getDetails(addedTestAuthorId).isEmpty());
    }

    @Test
    void whenGettingAllInstances_shouldReturnNonEmptyList() {
        // When
        List<AuthorDto> authors = this.authorService.getAll();

        // Then
        assertFalse(authors.isEmpty());
    }
}
