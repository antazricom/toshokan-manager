package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.data.repositories.AuthorDao;
import com.antazri.toshokan.manager.model.entity.Author;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class AuthorServiceImplTest {

    private final AuthorDao authorDao = mock(AuthorDao.class);
    private final AuthorServiceImpl authorService = new AuthorServiceImpl(authorDao);

    @Test
    void whenValidatingAddRequest_shouldThrowException() {
        // Given
        AuthorDto author = new AuthorDto();
        author.setFirstname("Lorem");

        // Then
        Assertions.assertThrows(BadRequestException.class, () -> authorService.create(author));
    }

    @Test
    void whenValidatingUpdateRequest_shouldThrowException() {
        // Given
        AuthorDto author = new AuthorDto(IdEncoder.encode(1), "Lorem", null, "");

        // When
        when(authorDao.existsById(anyInt())).thenReturn(true);
        when(authorDao.findById(anyInt())).thenReturn(Optional.of(new Author(1, "Ipsum", "Ipsum", "Ipsum")));

        // Then
        Assertions.assertThrows(BadRequestException.class, () -> authorService.update(author));
    }

    @Test
    void whenValidatingUpdateRequestWithUnknownAuthor_shouldThrowException() {
        // Given
        AuthorDto author = new AuthorDto(IdEncoder.encode(300), "Lorem", null, "Ipsum");

        // When
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        Assertions.assertThrows(IdNotValidException.class, () -> authorService.update(author));
    }

    @Test
    void whenGettingDetails_shouldReturnAsDto() {
        // Given
        when(authorDao.findById(anyInt())).thenReturn(Optional.of(new Author(1, "Lorem", null, "Ipsum")));

        // When
        Optional<AuthorDto> result = authorService.getDetails(1);

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertEquals(AuthorDto.class, result.get().getClass());
            assertTrue(result.get().getId().length() > 1);
            assertEquals("Lorem", result.get().getFirstname());
        });
    }

    @Test
    void whenNotFoundingAuthor_shouldReturnEmptyOptional() {
        // Given
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<AuthorDto> result = authorService.getDetails(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnEmptyLustIfNothingFound() {
        // Given
        when(authorDao.findByName(anyString())).thenReturn(Collections.emptyList());

        // When
        List<AuthorDto> result = authorService.searchByName("test");

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnListOfDTOs() {
        // Given
        List<Author> authors = new ArrayList<>();
        authors.add(new Author(1, "Lorem", null, "Ipsum"));
        when(authorDao.findByName(anyString())).thenReturn(authors);

        // When
        List<AuthorDto> dtos = authorService.searchByName("test");

        // Then
        assertAll(() -> {
            assertEquals(1, dtos.size());
            assertEquals(AuthorDto.class, dtos.get(0).getClass());
        });
    }

    @Test
    void whenRemovingWithUnknownId_shouldThrowException() {
        // When
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IdNotValidException.class, () -> authorService.remove(1));
    }
}