package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.*;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.BookService;
import com.antazri.toshokan.manager.data.repositories.AuthorDao;
import com.antazri.toshokan.manager.data.repositories.BookDao;
import com.antazri.toshokan.manager.data.repositories.BookProgressDao;
import com.antazri.toshokan.manager.data.repositories.CategoryDao;
import com.antazri.toshokan.manager.model.entity.Author;
import com.antazri.toshokan.manager.model.entity.Book;
import com.antazri.toshokan.manager.model.entity.BookProgress;
import com.antazri.toshokan.manager.model.entity.Category;
import com.antazri.toshokan.manager.model.utils.BookProgressStatus;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import com.antazri.toshokan.manager.technical.exceptions.IsbnNotValidException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class BookServiceImplTest {

    private final BookDao bookDao = mock(BookDao.class);
    private final AuthorDao authorDao = mock(AuthorDao.class);
    private final CategoryDao categoryDao = mock(CategoryDao.class);

    private final BookProgressDao bookProgressDao = mock(BookProgressDao.class);

    private final BookService bookService = new BookServiceImpl(bookDao, categoryDao, authorDao, bookProgressDao);

    @Test
    void whenIdIsNotFound_shouldReturnEmptyOptional() {
        // Given
        when(bookDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<BookDetailsDto> result = bookService.getDetails(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingById_shouldReturnDtoWithMaskedId() {
        // Given
        Book book = new Book(
                1,
                "Test",
                List.of(new Author(1, "Lorem", "Ipsum")),
                2000,
                "1234567891123",
                100,
                Book.Lang.FR,
                "edition",
                "summary",
                new Category(1, "Test"));

        when(bookDao.findById(anyInt())).thenReturn(Optional.of(book));

        // When
        Optional<BookDetailsDto> result = bookService.getDetails(1);

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertFalse(result.get().getId().isBlank());
            assertEquals("Test", result.get().getTitle());
            assertEquals("1234567891123", result.get().getIsbn());
            assertEquals(BookDto.class, result.get().getClass());
        });
    }

    @Test
    void whenSearchingWithWrongIsbn_shouldThrowException() {
        // Then
        assertThrows(IsbnNotValidException.class, () -> bookService.getByIsbn("123"));
    }

    @Test
    void whenIsbnIsNotFound_shouldReturnEmptyOptional() {
        // Given
        when(bookDao.findByIsbn(anyString())).thenReturn(Optional.empty());

        // When
        Optional<BookDto> result = bookService.getByIsbn("1234567891123");

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenSearchingByIsbn_shouldReturnInstanceAsDto() {
        // Given
        Book book = new Book(
                1,
                "Test",
                List.of(new Author(1, "Lorem", "Ipsum")),
                2000,
                "1234567891123",
                100,
                Book.Lang.FR,
                "edition",
                "summary",
                new Category(1, "Test"));

        when(bookDao.findByIsbn(anyString())).thenReturn(Optional.of(book));

        // When
        Optional<BookDto> result = bookService.getByIsbn("1234567891123");

        // Then
        assertAll(() -> {
            assertTrue(result.isPresent());
            assertFalse(result.get().getId().isBlank());
            assertEquals("Test", result.get().getTitle());
            assertEquals("1234567891123", result.get().getIsbn());
            assertEquals(BookDto.class, result.get().getClass());
        });
    }

    @Test
    void whenSearchingByTitle_shouldReturnDtoList() {
        // Given
        List<Book> books = new ArrayList<>();
        Book book = new Book(
                1,
                "Test",
                List.of(new Author(1, "Lorem", "Ipsum")),
                2000,
                "1234567891123",
                100,
                Book.Lang.FR,
                "edition",
                "summary",
                new Category(1, "Test"));
        books.add(book);

        when(bookDao.findByTitle(anyString())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByTitle("test");

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenSearchingByPublicationYear_shouldReturnDtoList() {
        // Given
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1,
                "Test",
                List.of(new Author(1, "Lorem", "Ipsum")),
                2000,
                "1234567891123",
                100,
                Book.Lang.FR,
                "edition",
                "summary",
                new Category(1, "Test")));

        when(bookDao.findByPublicationYear(anyInt())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByPublicationYear(2000);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenSearchingWithNotValidAuthor_shouldReturnEmptyList() {
        // Given
        when(authorDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        List<BookDto> result = bookService.getByAuthor(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingByAuthor_shouldReturnListAsDto() {
        // Given
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1,
                "Test",
                List.of(new Author(1, "Lorem", "Ipsum")),
                2000,
                "1234567891123",
                100,
                Book.Lang.FR,
                "edition",
                "summary",
                new Category(1, "Test")));

        when(authorDao.findById(anyInt())).thenReturn(Optional.of(new Author(1, "Lorem", "Ipsum")));
        when(bookDao.findByAuthorsContains(any())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByAuthor(1);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenSearchingWithNotValidCategory_shouldReturnEmptyList() {
        // Given
        when(categoryDao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        List<BookDto> result = bookService.getByCategory(1);

        // Then
        assertTrue(result.isEmpty());
    }

    @Test
    void whenGettingByCategory_shouldReturnListAsDto() {
        // Given
        List<Book> books = new ArrayList<>();
        Book book = new Book(
                1,
                "Test",
                List.of(new Author(1, "Lorem", "Ipsum")),
                2000,
                "1234567891123",
                100,
                Book.Lang.FR,
                "edition",
                "summary",
                new Category(1, "Test"));
        books.add(book);

        when(categoryDao.findById(anyInt())).thenReturn(Optional.of(new Category(1, "Lorem")));
        when(bookDao.findByCategory(any())).thenReturn(books);

        // When
        List<BookDto> result = bookService.getByCategory(1);

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenGettingAllinstances_shouldReturnListAsDto() {
        // Given
        List<Book> books = new ArrayList<>();
        Book book = new Book(
                1,
                "Test",
                List.of(new Author(1, "Lorem", "Ipsum")),
                1900,
                "1234567891123",
                100,
                Book.Lang.FR,
                "edition",
                "summary",
                new Category(1, "Test"));
        books.add(book);

        when(bookDao.findAll()).thenReturn(books);

        // When
        List<BookDto> result = bookService.getAll();

        // Then
        assertAll(() -> {
            assertEquals(1, result.size());
            assertFalse(result.get(0).getId().isBlank());
            assertEquals("Test", result.get(0).getTitle());
            assertEquals("1234567891123", result.get(0).getIsbn());
            assertEquals(BookDto.class, result.get(0).getClass());
        });
    }

    @Test
    void whenAddingWithNotValidIsbn_shouldThrowException() {
        // Given
        BookDto book = new BookDto(
                "Test",
                List.of(new AuthorDto(IdEncoder.encode(1), "Lorem", null, "Ipsum")),
                2000,
                "145",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), "Test", "Test")
        );

        // When
        when(authorDao.existsById(anyInt())).thenReturn(true);
        when(categoryDao.existsById(anyInt())).thenReturn(true);

        // Then
        assertThrows(IsbnNotValidException.class, () -> bookService.create(book));
    }

    @Test
    void whenAddingWithNotValidTitle_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "",
                List.of(new AuthorDto(IdEncoder.encode(1), "Lorem", "", "Ipsum")),
                2000,
                "1234567891123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), "Test", "Test")
        );

        // Then
        assertThrows(BadRequestException.class, () -> bookService.create(book));
    }

    @Test
    void whenAddingWithNotValidYear_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                "Test",
                List.of(new AuthorDto(IdEncoder.encode(1), "Lorem", "", "Ipsum")),
                2500,
                "1234567891123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), "Test", "Test")
        );

        // Then
        assertThrows(BadRequestException.class, () -> bookService.create(book));
    }

    @Test
    void whenUpdatingWithNotValidIsbn_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                IdEncoder.encode(1),
                "Test",
                List.of(new AuthorDto(IdEncoder.encode(1), "Lorem", "", "Ipsum")),
                2000,
                "145",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), "Test", "Test")
        );

        // Then
        assertThrows(IdNotValidException.class, () -> bookService.create(book));
    }

    @Test
    void whenUpdatingWithNotValidTitle_shouldThrowException() {
        // When
        BookDto book = new BookDto(
                IdEncoder.encode(1),
                "",
                List.of(new AuthorDto(IdEncoder.encode(1), "Lorem", "", "Ipsum")),
                2000,
                "1234567891123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), "Test", "Test")
        );

        // Then
        assertThrows(BadRequestException.class, () -> bookService.create(book));
    }

    @Test
    void whenUpdatingWithNotValidYear_shouldThrowException() {
        // Given
        BookDto book = new BookDto(
                IdEncoder.encode(2),
                "Test",
                List.of(new AuthorDto(IdEncoder.encode(1), "Lorem", "", "Ipsum")),
                2500,
                "1234567891123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), "Test", "Test")
        );

        // When
        when(bookDao.existsById(anyInt())).thenReturn(true);

        // Then
        assertThrows(BadRequestException.class, () -> bookService.update(book));
    }

    @Test
    void whenAddingWithNotValidCategoryId_shouldThrowException() {
        // Given
        BookDto book = new BookDto(
                "Test",
                List.of(new AuthorDto(IdEncoder.encode(1), "Lorem", "", "Ipsum")),
                Year.now().getValue(),
                "1234567891123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), "Test", "Test")
        );

        // When
        when(categoryDao.existsById(anyInt())).thenReturn(false);

        // Then
        assertThrows(IdNotValidException.class, () -> bookService.create(book));
    }

    @Test
    void whenFindingNoBookWhileRemoving_shouldThrowException() {
        // When
        when(bookDao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IdNotValidException.class, () -> bookService.remove(1));
    }

    @Test
    void whenRemovingBook_shouldRemoveQuietly() {
        // When
        when(bookDao.findById(anyInt()))
                .thenReturn(
                        Optional.of(
                                new Book(
                                        5,
                                        "Test",
                                        List.of(new Author(5, "Lorem", "Ipsum")),
                                        2000,
                                        "1234567891123",
                                        100,
                                        Book.Lang.FR,
                                        "edition",
                                        "summary",
                                        new Category(5, "Test", "Test")
                                )
                        )
                );
        doNothing().when(bookDao).delete(any(Book.class));

        // Then
        assertDoesNotThrow(() -> bookService.remove(1));
    }

    @Test
    void whenUpdatingBookProgress_shouldReturnUpdatedEntity() throws Exception {
        // Given
        Book entity = new Book();
        entity.setId(1);
        entity.setNbPages(200);

        BookProgressDto dto = new BookProgressDto(IdEncoder.encode(1), 100, "READING", IdEncoder.encode(1));

        // When
        when(bookProgressDao.save(any())).thenAnswer(bp -> bp.getArguments()[0]);
        when(bookDao.findById(any())).thenReturn(Optional.of(entity));
        BookProgressDto updatedProgress = this.bookService.updateProgress(dto);

        // Then
        assertAll(() -> {
            assertEquals(BookProgressStatus.READING.getName(), updatedProgress.getStatus());
            assertEquals(100, updatedProgress.getNbReadPages());
        });
    }

    @Test
    void whenUpdatingBookProgressWithInvalidNbReadPages_shouldThrowException() {
        // Given
        Book entity = new Book();
        entity.setId(1);
        entity.setNbPages(200);

        BookProgress bookProgress = new BookProgress(1, 0, BookProgressStatus.NEW, entity);
        BookProgressDto dto = new BookProgressDto(IdEncoder.encode(1), 1000, "READING", IdEncoder.encode(1));

        // When
        when(bookProgressDao.save(any())).thenReturn(bookProgress);
        when(bookDao.findById(any())).thenReturn(Optional.of(entity));

        // Then
        assertThrows(Exception.class, () -> this.bookService.updateProgress(dto));
    }

    @Test
    void whenUpdatingBookProgressWithMaxNbReadPages_shouldSetStatusAtComplete() throws Exception {
        // Given
        Book entity = new Book();
        entity.setId(1);
        entity.setNbPages(200);

        BookProgressDto dto = new BookProgressDto(IdEncoder.encode(1), 200, "READING", IdEncoder.encode(1));

        // When
        when(bookProgressDao.save(any())).thenAnswer(bp -> bp.getArguments()[0]);
        when(bookDao.findById(any())).thenReturn(Optional.of(entity));
        BookProgressDto updatedProgress = this.bookService.updateProgress(dto);

        // Then
        assertEquals(BookProgressStatus.COMPLETE.getName(), updatedProgress.getStatus());
    }
}