package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.ArtistDto;
import com.antazri.toshokan.manager.data.repositories.ArtistDao;
import com.antazri.toshokan.manager.model.entity.Artist;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ArtistServiceImplTest {

    private final ArtistDao dao = mock(ArtistDao.class);
    private final ArtistServiceImpl artistService = new ArtistServiceImpl(dao);

    @Test
    void whenAddingWithNotValidRequest_shouldThrowException() {
        // Given
        ArtistDto artist = new ArtistDto();
        artist.setName("    ");

        // Then
        assertThrows(BadRequestException.class, () -> artistService.create(artist));
    }

    @Test
    void whenUpdatingWithNotValidRequest_shouldThrowException() {
        // Given
        ArtistDto artist = new ArtistDto();
        artist.setName("    ");

        // Then
        assertThrows(DecodingException.class, () -> artistService.update(artist));
    }

    @Test
    void whenGettingDetails_shouldReturnAsDto() {
        // Given
        when(dao.findById(anyInt())).thenReturn(Optional.of(new Artist(1, "Test")));

        // When
        Optional<ArtistDto> details = artistService.getDetails(1);

        // Then
        assertAll(() -> {
            assertTrue(details.isPresent());
            assertEquals(ArtistDto.class, details.get().getClass());
            assertTrue(details.get().getId().length() > 1);
            assertEquals("Test", details.get().getName());
        });
    }

    @Test
    void whenNotFoundingArtist_shouldReturnEmptyOptional() {
        // Given
        when(dao.findById(anyInt())).thenReturn(Optional.empty());

        // When
        Optional<ArtistDto> details = artistService.getDetails(1);

        // Then
        assertTrue(details.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnEmptyLustIfNothingFound() {
        // Given
        when(dao.findByName(anyString())).thenReturn(Collections.emptyList());

        // When
        List<ArtistDto> artists = artistService.searchByName("test");

        // Then
        assertTrue(artists.isEmpty());
    }

    @Test
    void whenSearchingByName_shouldReturnListOfDTOs() {
        // Given
        List<Artist> artists = new ArrayList<>();
        artists.add(new Artist(1, "Test 1"));
        when(dao.findByName(anyString())).thenReturn(artists);

        // When
        List<ArtistDto> dtos = artistService.searchByName("test");

        // Then
        assertAll(() -> {
            assertEquals(1, dtos.size());
            assertEquals(ArtistDto.class, dtos.get(0).getClass());
        });
    }

    @Test
    void whenRemovingWithUnknownId_shouldThrowException() {
        // When
        when(dao.findById(anyInt())).thenReturn(Optional.empty());

        // Then
        assertThrows(IdNotValidException.class, () -> artistService.remove(1));
    }
}