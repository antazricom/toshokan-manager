package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.ArtistDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.ArtistService;
import com.antazri.toshokan.manager.technical.annotations.test.IntegrationSpringBootTest;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import net.bytebuddy.utility.RandomString;
import org.bson.assertions.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@IntegrationSpringBootTest
public class ArtistServiceImplIntegrationTest {

    @Autowired
    private ArtistService artistService;

    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenAddingNewArtistDtoInstance_shouldReturnEntityWithId() throws DecodingException {
        // Given
        ArtistDto artistDto = new ArtistDto(RandomString.make(8));

        // When
        ArtistDto a = this.artistService.create(artistDto);

        // Then
        Assertions.assertNotNull(a.getId());
    }

    @Test
    void whenUpdatingArtist_shouldReturnUpdatedEntityWithId() throws DecodingException {
        // Given
        String newName = "MYNEWGROUP";
        ArtistDto details = this.artistService.getDetails(3).orElseThrow();
        details.setName(newName);

        // When
        ArtistDto a = this.artistService.update(details);

        // Then
        assertEquals(newName, a.getName());
    }

    @Test
    void whenRemovingArtist_shouldReturnEmptyOptionalFromDetailsEndpoint() throws DecodingException {
        // Given
        ArtistDto testArtist = new ArtistDto(RandomString.make(4));

        // When
        ArtistDto addedTestArtist = this.artistService.create(testArtist);
        int addedTestArtistId = IdEncoder.decode(addedTestArtist.getId());

        // Then
        assertDoesNotThrow(() -> this.artistService.remove(addedTestArtistId));
        assertTrue(this.artistService.getDetails(addedTestArtistId).isEmpty());
    }

    @Test
    void whenGettingAllInstances_shouldReturnNonEmptyList() {
        // When
        List<ArtistDto> artists = this.artistService.getAll();

        // Then
        assertFalse(artists.isEmpty());
    }
}
