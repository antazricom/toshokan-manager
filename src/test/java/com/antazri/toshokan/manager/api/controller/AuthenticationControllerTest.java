package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.security.auth.AuthenticationRequest;
import com.antazri.toshokan.manager.technical.annotations.test.ControllerSpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled("Waiting for SpringSecurity update")
@ControllerSpringBootTest
public class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenAuthenticationIsValid_shouldReturn200() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/auth")
                                .content(this.objectMapper.writeValueAsString(new AuthenticationRequest("root@test.com", "admin")))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }
}
