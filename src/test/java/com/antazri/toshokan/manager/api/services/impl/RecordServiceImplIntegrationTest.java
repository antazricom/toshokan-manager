package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.RecordDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.RecordService;
import com.antazri.toshokan.manager.model.entity.Record;
import com.antazri.toshokan.manager.technical.annotations.test.IntegrationSpringBootTest;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.bson.assertions.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@IntegrationSpringBootTest
class RecordServiceImplIntegrationTest {

    @Autowired
    private RecordService recordService;

    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenAddingNewExpenseEntity_shouldReturnWithId() throws DecodingException {
        // Given
        RecordDto recordDto = new RecordDto(
                LocalDateTime.now(),
                1,
                "lorem",
                10.0,
                0.0,
                "BOOK",
                IdEncoder.encode(5)
        );

        // When
        RecordDto addedExpense = this.recordService.create(recordDto);

        // Then
        Assertions.assertNotNull(addedExpense.getId());
    }

    @Test
    void whenUpdatingExistingEntity_shouldReturnUpdatedExpense() throws DecodingException {
        // Given
        double newAmount = 100.0;
        RecordDto recordDto = this.recordService.getBookRecord(2).orElseThrow();
        recordDto.setExpense(newAmount);

        // When
        RecordDto updatedExpense = this.recordService.update(recordDto);

        // Then
        assertEquals(newAmount, updatedExpense.getExpense());
    }

    @Test
    void whenRemovingEntity_shouldReturnEmptyOptional() throws DecodingException {
        // When
        RecordDto recordDto = new RecordDto(
                LocalDateTime.now(),
                1,
                "lorem",
                10.0,
                0.0,
                "BOOK",
                IdEncoder.encode(5)
        );
        RecordDto addedExpense = this.recordService.create(recordDto);


        // Then
        assertDoesNotThrow(() -> this.recordService.remove(IdEncoder.decode(addedExpense.getId())));
        assertTrue(this.recordService.getBookRecord(IdEncoder.decode(addedExpense.getId())).isEmpty());
    }

    @Test
    void whenGettingAllInstances_shouldReturnNotEmptyList() {
        // When
        List<RecordDto> expenses = this.recordService.getAll();

        // Then
        assertFalse(expenses.isEmpty());
    }

    @Test
    void whenFindingExpenseWithUnknownId_shouldReturnEmptyOptional() {
        // When
        Optional<RecordDto> expense = this.recordService.getBookRecord(100);

        // Then
        assertTrue(expense.isEmpty());
    }

    @Test
    void whenFindingByBookType_shouldReturnBooksExpenses() {
        // When
        List<RecordDto> booksExpenses = this.recordService.getBooksExpenses();

        // Then
        assertTrue(booksExpenses.stream().allMatch(e -> Record.Type.BOOK.name().equals(e.getType())));
    }

    @Test
    void whenFindingByVinylType_shouldReturnVinylsExpenses() {
        // When
        List<RecordDto> vinylsExpenses = this.recordService.getVinylsExpenses();

        // Then
        assertTrue(vinylsExpenses.stream().allMatch(e -> Record.Type.VINYL.name().equals(e.getType())));
    }
}