package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.ArtistDto;
import com.antazri.toshokan.manager.api.dto.CategoryDto;
import com.antazri.toshokan.manager.api.dto.VinylDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.VinylService;
import com.antazri.toshokan.manager.technical.annotations.test.IntegrationSpringBootTest;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.time.Year;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@IntegrationSpringBootTest
public class VinylServiceImplIntegrationTest {

    @Autowired
    private VinylService vinylService;

    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenAddingNewEntity_shouldReturnWithId() throws DecodingException {
        // Given
        VinylDto vinylDto = new VinylDto(
                RandomString.make(4),
                new ArtistDto(IdEncoder.encode(1), RandomString.make(4)),
                Year.now().getValue(),
                new CategoryDto(IdEncoder.encode(1),
                        RandomString.make(4),
                        RandomString.make(4))
        );

        // When
        VinylDto addedVinyl = this.vinylService.create(vinylDto);

        // Then
        assertNotNull(addedVinyl.getId());
    }

    @Test
    void whenUpdatingEntity_shouldReturnUpdatedObject() throws DecodingException {
        // Given
        String title = "NEWVINYLTITLE";
        VinylDto vinylDto = this.vinylService.getDetails(11).orElseThrow();
        vinylDto.setTitle(title);

        // When
        VinylDto updatedVinyl = this.vinylService.update(vinylDto);

        // Then
        assertEquals(title, updatedVinyl.getTitle());
    }

    @Test
    void whenRemovingEntity_shouldReturnEmptyOptional() throws DecodingException {
        // When
        VinylDto vinylDto = new VinylDto(
                RandomString.make(4),
                new ArtistDto(IdEncoder.encode(1), RandomString.make(4)),
                Year.now().getValue(),
                new CategoryDto(IdEncoder.encode(1),
                        RandomString.make(4),
                        RandomString.make(4))
        );
        VinylDto addedVinyl = this.vinylService.create(vinylDto);

        // Then
        assertDoesNotThrow(() -> this.vinylService.remove(IdEncoder.decode(addedVinyl.getId())));
        assertTrue(this.vinylService.getDetails(IdEncoder.decode(addedVinyl.getId())).isEmpty());
    }

    @Test
    void whenGettingAllInstances_shouldReturnNonEmptyList() {
        // When
        List<VinylDto> vinyls = this.vinylService.getAll();

        // Then
        assertFalse(vinyls.isEmpty());
    }
}
