package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.RecordDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.RecordService;
import com.antazri.toshokan.manager.data.repositories.RecordDao;
import com.antazri.toshokan.manager.data.repositories.ItemDao;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
class RecordServiceImplTest {

    private final RecordDao recordDao = mock(RecordDao.class);
    private final ItemDao itemDao = mock(ItemDao.class);
    private final RecordService recordService = new RecordServiceImpl(recordDao, itemDao);

    @Test
    void whenAddingInvalidExpense_shouldThrowBadRequestException() {
        // When
        RecordDto recordDto = new RecordDto(
                LocalDateTime.now(),
                1,
                "lorem",
                -100.0,
                0.0,
                null,
                IdEncoder.encode(7)
        );

        // Then
        Assertions.assertThrows(BadRequestException.class, () -> this.recordService.create(recordDto));
    }

    @Test
    void whenUpdatingInvalidExpense_shouldThrowBadRequestException() {
        // When
        RecordDto recordDto = new RecordDto(
                IdEncoder.encode(2),
                LocalDateTime.now(),
                1,
                "lorem",
                -100.0,
                0.0,
                null,
                IdEncoder.encode(7)
        );

        when(recordDao.existsById(2)).thenReturn(true);

        // Then
        Assertions.assertThrows(BadRequestException.class, () -> this.recordService.update(recordDto));
    }

    @Test
    void whenUpdatingInvalidIdExpense_shouldThrowIdNotValidException() {
        // When
        RecordDto recordDto = new RecordDto(
                IdEncoder.encode(2),
                LocalDateTime.now(),
                1,
                "lorem",
                -100.0,
                0.0,
                null,
                IdEncoder.encode(7)
        );

        when(recordDao.existsById(any())).thenReturn(false);

        // Then
        Assertions.assertThrows(IdNotValidException.class, () -> this.recordService.update(recordDto));
    }
}