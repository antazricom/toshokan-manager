package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.*;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.utils.BookProgressStatus;
import com.antazri.toshokan.manager.technical.annotations.test.ControllerSpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Year;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled("Waiting for SpringSecurity update")
@ControllerSpringBootTest
class BookManagementControllerTest {

    private static final Class<BookDto> TYPE = BookDto.class;
    private static final String BASE_PATH = "/books";
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private Environment environment;

    @Test
    void contextLoads() {
        assertArrayEquals(new String[]{"test"}, environment.getActiveProfiles());
    }

    @Test
    void whenSendingRequestWithoutCredentials_shouldReturnHttp403() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenRequestingInstanceDetailsWithValidId_shouldReturnHttp200() throws Exception {
        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH + "/{id}", IdEncoder.encode(1))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        BookDto bookDto = objectMapper.readValue(result.getResponse().getContentAsString(), TYPE);

        assertEquals("1Q84 Tome 1", bookDto.getTitle());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenRequestingDetailsWithNotValidId_shouldReturnHttp404() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH + "{id}", IdEncoder.encode(1000))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenSendingARequestInstancesByCategory_shouldReturnHttp200() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH + "/category/{id}", IdEncoder.encode(1))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenSendingARequestInstancesByAuthor_shouldReturnHttp200() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH + "/author/{id}", IdEncoder.encode(1))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenSendingARequestInstancesByTitle_shouldReturnHttp200() throws Exception {
        String title = "test";
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH + "/title")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(title))
                ).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenSendingARequestForAllInstances_shouldReturnHttp200() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenCreatingNewInstance_shouldReturnHttp201() throws Exception {
        BookDetailsDto requestBody = new BookDetailsDto(
                RandomString.make(4),
                List.of(new AuthorDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4), RandomString.make(4))),
                Year.now().getValue(),
                "0123456789",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4)),
                null
        );

        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(requestBody))
                ).andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenUpdatingInstanceWithValidId_shouldReturnHttp200() throws Exception {
        BookDetailsDto requestBody = new BookDetailsDto(
                IdEncoder.encode(2),
                "RANDOMTITLE",
                List.of(new AuthorDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4), RandomString.make(4))),
                Year.now().getValue(),
                "0123456789123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4)),
                new BookProgressDto(IdEncoder.encode(1), 100, BookProgressStatus.NEW.getName(), IdEncoder.encode(2))
        );

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .put(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(requestBody))
                ).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        BookDto bookDto = objectMapper.readValue(result.getResponse().getContentAsString(), TYPE);

        assertEquals("RANDOMTITLE", bookDto.getTitle());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenUpdatingInstanceWithNotValidRequestBody_shouldReturnHttp400() throws Exception {
        BookDetailsDto requestBody = new BookDetailsDto(
                IdEncoder.encode(2),
                "",
                List.of(new AuthorDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4), RandomString.make(4))),
                Year.now().getValue(),
                "0123456789123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4)),
                new BookProgressDto(IdEncoder.encode(1), 100, BookProgressStatus.NEW.getName(), IdEncoder.encode(2))
        );

        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .put(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(requestBody))
                ).andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenUpdatingInstanceWithNotValidId_shouldReturnHttp400() throws Exception {
        BookDetailsDto requestBody = new BookDetailsDto(
                IdEncoder.encode(2000),
                "RANDOM",
                List.of(new AuthorDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4), RandomString.make(4))),
                Year.now().getValue(),
                "0123456789123",
                100,
                "fr",
                "edition",
                "summary",
                new CategoryDto(IdEncoder.encode(1), RandomString.make(4), RandomString.make(4)),
                new BookProgressDto(IdEncoder.encode(1), 100, BookProgressStatus.NEW.getName(), IdEncoder.encode(2000))
        );

        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .put(BASE_PATH)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(requestBody))
                ).andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = {"ADMIN"})
    void whenRemovingInstance_shouldReturnHttp200() throws Exception {
        this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(BASE_PATH + "/{id}", IdEncoder.encode(8))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk());
    }

}