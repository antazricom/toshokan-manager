package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.entity.Author;
import com.antazri.toshokan.manager.model.entity.Book;
import com.antazri.toshokan.manager.model.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BookDao extends JpaRepository<Book, Integer> {

    List<Book> findByTitle(String title);

    List<Book> findByAuthorsContains(Author author);

    List<Book> findByCategory(Category category);

    Optional<Book> findByIsbn(String isbn);

    List<Book> findByPublicationYear(int year);

    List<Book> findByCreatedAtAfter(LocalDateTime createdAt);
}
