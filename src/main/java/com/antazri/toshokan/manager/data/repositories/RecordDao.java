package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.entity.Item;
import com.antazri.toshokan.manager.model.entity.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecordDao extends JpaRepository<Record, Integer> {

    Optional<Record> findByItem(Item item);

    Optional<Record> findByItemId(Integer itemId);

    List<Record> findAllByType(Record.Type type);
}
