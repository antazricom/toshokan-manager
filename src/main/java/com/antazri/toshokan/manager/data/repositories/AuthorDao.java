package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuthorDao extends JpaRepository<Author, Integer> {

    @Query("SELECT a " +
            "FROM Author a " +
            "WHERE lower(concat(a.firstname, ' ', a.middlename, ' ', a.lastname)) LIKE lower(:name) " +
            "OR lower(concat(a.firstname, ' ', a.lastname)) LIKE lower(:name)")
    List<Author> findByName(String name);

}
