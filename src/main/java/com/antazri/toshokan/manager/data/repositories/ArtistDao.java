package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.entity.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArtistDao extends JpaRepository<Artist, Integer> {

    List<Artist> findByName(String name);

}
