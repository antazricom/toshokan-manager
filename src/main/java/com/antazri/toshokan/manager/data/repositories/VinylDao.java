package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.entity.Artist;
import com.antazri.toshokan.manager.model.entity.Category;
import com.antazri.toshokan.manager.model.entity.Vinyl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface VinylDao extends JpaRepository<Vinyl, Integer> {

    List<Vinyl> findByTitle(String title);

    List<Vinyl> findByArtist(Artist artist);

    List<Vinyl> findByPublicationYear(int year);

    List<Vinyl> findByPublicationYearBetween(int from, int to);

    List<Vinyl> findByCategory(Category category);

    List<Vinyl> findByCreatedAtAfter(LocalDateTime createdAt);
}
