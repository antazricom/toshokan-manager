package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.user.Login;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginDao extends JpaRepository<Login, Integer> {
    Optional<Login> findByEmail(String email);
}
