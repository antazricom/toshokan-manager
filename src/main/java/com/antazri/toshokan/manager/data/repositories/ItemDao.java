package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("itemDao")
public interface ItemDao extends JpaRepository<Item, Integer> {

}
