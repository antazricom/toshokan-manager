package com.antazri.toshokan.manager.data.repositories;

import com.antazri.toshokan.manager.model.entity.Book;
import com.antazri.toshokan.manager.model.entity.BookProgress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookProgressDao extends JpaRepository<BookProgress, Integer> {

    Optional<BookProgress> findByBook(Book book);
}
