package com.antazri.toshokan.manager.technical.exceptions;

public class IsbnNotValidException extends RuntimeException {

    private static final long serialVersionUID = -5148840206649897203L;

	public IsbnNotValidException() {
    }

    public IsbnNotValidException(String message) {
        super(message);
    }

    public IsbnNotValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public IsbnNotValidException(Throwable cause) {
        super(cause);
    }

    protected IsbnNotValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
