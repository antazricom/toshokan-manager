package com.antazri.toshokan.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = {"com.antazri.toshokan.manager"})
@EnableJpaRepositories("com.antazri.toshokan.manager.data.repositories")
@EnableMongoRepositories
public class ToshokanManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToshokanManagerApplication.class, args);
    }

}
