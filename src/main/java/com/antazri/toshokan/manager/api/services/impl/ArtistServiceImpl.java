package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.ArtistDto;
import com.antazri.toshokan.manager.api.dto.mapper.ArtistMapper;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.ArtistService;
import com.antazri.toshokan.manager.data.repositories.ArtistDao;
import com.antazri.toshokan.manager.model.entity.Artist;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("artistService")
public class ArtistServiceImpl implements ArtistService {

    private final static Logger logger = LogManager.getLogger(ArtistServiceImpl.class);

    private final ArtistDao artistDao;

    public ArtistServiceImpl(ArtistDao artistDao) {
        this.artistDao = artistDao;
    }

    @Override
    public Optional<ArtistDto> getDetails(int id) {
        final Optional<Artist> artist = artistDao.findById(id);

        if (artist.isEmpty()) {
            logger.error("Artist not found with id {}", id);
            return Optional.empty();
        }

        return artist.map(ArtistMapper::asDto);
    }

    @Override
    public List<ArtistDto> searchByName(String name) {
        final List<Artist> artists = artistDao.findByName(name.trim());

        if (!artists.isEmpty()) {
            return artists.stream().map(ArtistMapper::asDto).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<ArtistDto> getAll() {
        return artistDao.findAll().stream()
                .map(ArtistMapper::asDto)
                .collect(Collectors.toList());
    }

    @Override
    public ArtistDto create(ArtistDto artist) throws DecodingException {
        if (!validCreateBodyRequest(artist)) {
            logger.error("Artist not valid: {}", artist);
            throw new BadRequestException("Artist is not valid");
        }

        logger.info("Add {}", artist);
        Artist artistDb = ArtistMapper.asEntity(artist);

        return ArtistMapper.asDto(artistDao.save(artistDb));
    }

    private boolean validCreateBodyRequest(ArtistDto artist) {
        return !artist.getName().isBlank();
    }

    @Override
    public ArtistDto update(ArtistDto artist) throws DecodingException {
        if (!artistDao.existsById(IdEncoder.decode(artist.getId()))) {
            logger.error("Update attempt on unknown entity : {}", artist);
            throw new IdNotValidException("Artist to update is unknown");
        }

        if (!validUpdateBodyRequest(artist)) {
            logger.error("Artist to update is not valid: {}", artist);
            throw new BadRequestException("Artist to update is not valid");
        }

        logger.info("Update {}", artist);
        Artist artistDb = ArtistMapper.asEntity(artist);

        return ArtistMapper.asDto(artistDao.save(artistDb));
    }

    private boolean validUpdateBodyRequest(ArtistDto artist) {
        return Stream.of(artist.getId(), artist.getName()).noneMatch(Objects::isNull)
                && Stream.of(artist.getId(), artist.getName()).noneMatch(String::isBlank);
    }

    @Override
    public boolean remove(int id) {
        final Optional<Artist> artist = artistDao.findById(id);
        if (artist.isEmpty()) {
            logger.error("Artist not found with id: {}", id);
            throw new IdNotValidException("Artist to remove not found");
        }

        logger.info("Remove Artist {}", artist.get());
        artistDao.delete(artist.get());
        return true;
    }
}
