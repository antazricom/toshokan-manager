package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.*;
import com.antazri.toshokan.manager.api.dto.mapper.BookDetailsMapper;
import com.antazri.toshokan.manager.api.dto.mapper.BookMapper;
import com.antazri.toshokan.manager.api.dto.mapper.BookProgressMapper;
import com.antazri.toshokan.manager.api.dto.mapper.BookSampleMapper;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.BookService;
import com.antazri.toshokan.manager.data.repositories.AuthorDao;
import com.antazri.toshokan.manager.data.repositories.BookDao;
import com.antazri.toshokan.manager.data.repositories.BookProgressDao;
import com.antazri.toshokan.manager.data.repositories.CategoryDao;
import com.antazri.toshokan.manager.model.entity.Author;
import com.antazri.toshokan.manager.model.entity.Book;
import com.antazri.toshokan.manager.model.entity.BookProgress;
import com.antazri.toshokan.manager.model.entity.Category;
import com.antazri.toshokan.manager.model.utils.BookProgressStatus;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import com.antazri.toshokan.manager.technical.exceptions.IsbnNotValidException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

@Service
@Qualifier("bookService")
public class BookServiceImpl implements BookService {

    private static final Logger logger = LogManager.getLogger(BookServiceImpl.class);
    private static final Pattern ISBN = Pattern.compile("(?=(?:\\D*\\d){10}(?:(?:\\D@*\\d){3})?$)");
    private final BookDao bookDao;
    private final CategoryDao categoryDao;
    private final AuthorDao authorDao;

    private final BookProgressDao bookProgressDao;

    public BookServiceImpl(BookDao bookDao, CategoryDao categoryDao, AuthorDao authorDao, BookProgressDao bookProgressDao) {
        this.bookDao = bookDao;
        this.categoryDao = categoryDao;
        this.authorDao = authorDao;
        this.bookProgressDao = bookProgressDao;
    }

    @Override
    public Optional<BookDetailsDto> getDetails(int id) {
        final Optional<Book> dbBook = this.bookDao.findById(id);

        if (dbBook.isEmpty()) {
            logger.info("Book not found with id {}", id);
            return Optional.empty();
        }

        logger.info("Get book {} details", id);

        return dbBook.map(BookDetailsMapper::asDto);
    }

    @Override
    public Optional<BookDto> getByIsbn(String isbn) {
        checkISBN(isbn);

        logger.info("Get book details for ISBN {}", isbn);
        final Optional<Book> bookDb = this.bookDao.findByIsbn(isbn);

        if (bookDb.isEmpty()) {
            return Optional.empty();
        } else {
            return bookDb.map(BookMapper::asDto);
        }
    }

    @Override
    public List<BookDto> getByTitle(String title) {
        final List<Book> dbBooks = new ArrayList<>(this.bookDao.findByTitle(title.trim()));
        return dbBooks.stream().map(BookMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getByPublicationYear(int year) {
        final List<Book> dbBooks = new ArrayList<>(this.bookDao.findByPublicationYear(year));
        return dbBooks.stream().map(BookMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getByAuthor(int authorId) {
        Optional<Author> authorOpt = authorDao.findById(authorId);

        if (authorOpt.isEmpty()) {
            logger.error("No author found with id {}", authorId);
            return Collections.emptyList();
        }

        return this.bookDao.findByAuthorsContains(authorOpt.get()).stream().map(BookMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getByCategory(int categoryId) {
        Optional<Category> categoryOpt = categoryDao.findById(categoryId);

        if (categoryOpt.isEmpty()) {
            logger.error("No Category found with id {}", categoryId);
            return Collections.emptyList();
        }

        return this.bookDao.findByCategory(categoryOpt.get()).stream().map(BookMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getAll() {
        return this.bookDao.findAll().stream()
                .map(BookMapper::asDto)
                .collect(Collectors.toList());
    }

    @Override
    public BookDto create(BookDto book) throws DecodingException {
        if (!validCreateBookRequest(book)) {
            logger.error("Request to add new book is not valid");
            throw new BadRequestException("Request to add new book is not valid");
        }

        checkAuthors(book.getAuthors());
        checkCategory(book.getCategory());
        checkISBN(book.getIsbn());

        logger.info("Add Book {}", book);

        Book added = this.bookDao.save(BookMapper.asEntity(book));

        this.bookProgressDao.save(new BookProgress(0, BookProgressStatus.NEW, added));
        logger.info("Created first progress associated with book {}", added);

        return BookMapper.asDto(added);
    }

    private boolean validCreateBookRequest(BookDto book) {
        return Stream.of(book.getAuthors(), book.getCategory(), book.getIsbn(), book.getPublicationYear(), book.getTitle())
                .noneMatch(Objects::isNull)
                && !book.getTitle().isBlank()
                && (book.getPublicationYear() > 0 && book.getPublicationYear() <= LocalDate.now().getYear());
    }

    @Override
    public BookDto update(BookDto book) throws DecodingException {
        if (!this.bookDao.existsById(IdEncoder.decode(book.getId()))) {
            logger.error("Book not found with id {} ({})", book.getId(), IdEncoder.decode(book.getId()));
            throw new IdNotValidException("Book id not valid or not found");
        }

        if (!validUpdateBookRequest(book)) {
            logger.error("Book to update is not valid");
            throw new BadRequestException("Book to update is not valid");
        }

        checkAuthors(book.getAuthors());
        checkCategory(book.getCategory());
        checkISBN(book.getIsbn());

        logger.info("Update Book {}", book);
        Book updated = BookMapper.asEntity(book);

        return BookMapper.asDto(this.bookDao.save(updated));
    }

    private boolean validUpdateBookRequest(BookDto book) {
        return Stream.of(book.getAuthors(), book.getCategory(), book.getIsbn(), book.getPublicationYear(), book.getTitle())
                .noneMatch(Objects::isNull)
                && !book.getTitle().isBlank()
                && (book.getPublicationYear() > 0 && book.getPublicationYear() <= LocalDate.now().getYear());
    }

    private void checkISBN(String isbn) {
        if (!ISBN.matcher(isbn).find()) {
            logger.error("ISBN {} is not valid", isbn);
            throw new IsbnNotValidException("ISBN is not valid");
        }
    }

    private void checkAuthors(List<AuthorDto> authors) throws DecodingException {
        for (AuthorDto author : authors) {
            if (!this.authorDao.existsById(IdEncoder.decode(author.getId()))) {
                logger.error("No author found with id {}", author.getId());
                throw new IdNotValidException("Author is not valid");
            }
        }
    }

    private void checkCategory(CategoryDto category) throws DecodingException {
        if (category.getId() != null
                && !this.categoryDao.existsById(IdEncoder.decode(category.getId()))) {
            logger.error("No category found with id {}", category.getId());
            throw new IdNotValidException("Category is not valid");
        }
    }

    @Override
    public boolean remove(int id) {
        final Optional<Book> bookOpt = this.bookDao.findById(id);

        if (bookOpt.isEmpty()) {
            logger.error("Book not found with id: {}", id);
            throw new IdNotValidException("No Book found");
        }

        final Optional<BookProgress> progress = this.bookProgressDao.findByBook(bookOpt.get());

        if (progress.isPresent()) {
            logger.info("Book {} progress {} removed", bookOpt.get(), progress.get());
            this.bookProgressDao.delete(progress.get());
        } else {
            logger.info("Book {} to be removed has no progress", bookOpt.get());
        }

        this.bookDao.delete(bookOpt.get());
        logger.info("Book {} has been removed", bookOpt.get());
        return true;
    }

    @Override
    public BookProgressDto updateProgress(BookProgressDto progress) throws Exception {
        Book book = this.bookDao.findById(IdEncoder.decode(progress.getBookId())).orElseThrow();
        if (progress.getNbReadPages() >= 0 && book.getNbPages() >= progress.getNbReadPages()) {
            if (progress.getNbReadPages() == book.getNbPages()) {
                progress.setStatus(BookProgressStatus.COMPLETE.getName());
            }

            BookProgress updatedProgress = this.bookProgressDao.save(BookProgressMapper.asEntity(progress));
            return BookProgressMapper.asDto(updatedProgress);
        }

        logger.error("Error while updating book progress");
        throw new Exception("Error while updating book progress");
    }

    @Override
    public List<BookSampleDto> getAllSampleBooks() {
        return this.bookDao.findAll().stream()
                .map(BookSampleMapper::asDto)
                .sorted(Comparator.comparing(BookSampleDto::getTitle))
                .toList();
    }

    @Override
    public Map<String, List<BookSampleDto>> getAllSortedSampleBooks() {
        return this.bookDao.findAll().stream()
                .map(BookSampleMapper::asDto)
                .collect(groupingBy(b -> String.valueOf(b.getTitle().toLowerCase().charAt(0))));
    }

    @Override
    public List<BookSampleDto> findBooksCreatedFrom(LocalDateTime date) {
        return this.bookDao.findByCreatedAtAfter(date).stream()
                .map(BookSampleMapper::asDto)
                .sorted(Comparator.comparing(BookSampleDto::getCreatedAt).reversed())
                .toList();
    }
}
