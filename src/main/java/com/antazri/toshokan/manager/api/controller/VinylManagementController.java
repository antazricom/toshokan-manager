package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.VinylDto;
import com.antazri.toshokan.manager.api.dto.VinylSampleDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.VinylService;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/vinyls")
@CrossOrigin({"http://localhost:4200"})
public class VinylManagementController {

    private static final Logger logger = LogManager.getLogger(VinylManagementController.class);

    private final VinylService vinylService;

    public VinylManagementController(VinylService vinylService) {
        this.vinylService = vinylService;
    }

    @GetMapping("/sample")
    public ResponseEntity<List<VinylSampleDto>> getAllSampleVinyls() {
        logger.info("Request get all vinyls as samples");
        return ResponseEntity.ok(vinylService.getAllVinylSamples());
    }

    @GetMapping("/sample/sorted")
    public ResponseEntity<Map<String, List<VinylSampleDto>>> getAllSortedSampleVinyls() {
        logger.info("Request get all sorted vinyls as samples");
        return ResponseEntity.ok(vinylService.getAllSortedVinylSamples());
    }

    @GetMapping
    public ResponseEntity<List<VinylDto>> getAllVinyls() {
        logger.info("Request get all vinyls");
        return ResponseEntity.ok(vinylService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<VinylDto> getVinylDetails(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get vinyl details {}", id);
        VinylDto vinyl = vinylService.getDetails(IdEncoder.decode(id))
                .orElseThrow(() -> new NoSuchElementException("No vinyl found"));
        return ResponseEntity.ok(vinyl);
    }

    @GetMapping("/title")
    public ResponseEntity<List<VinylDto>> getVinylsByTitle(@RequestBody String title) {
        logger.info("Request get vinyls by title {}", title);
        return ResponseEntity.ok(vinylService.getByTitle(title));
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<List<VinylDto>> getVinylsByCategory(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get vinyls by category {}", id);
        return ResponseEntity.ok(vinylService.getByCategory(IdEncoder.decode(id)));
    }

    @GetMapping("/artist/{id}")
    public ResponseEntity<List<VinylDto>> getVinylsByArtist(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get vinyls by artist {}", id);
        return ResponseEntity.ok(vinylService.getByArtist(IdEncoder.decode(id)));
    }

    @PostMapping
    public ResponseEntity<VinylDto> postAddVinyl(@RequestBody VinylDto vinyl) throws DecodingException {
        logger.info("Request add vinyl {}", vinyl);
        return new ResponseEntity<>(this.vinylService.create(vinyl), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<VinylDto> putUpdateVinyl(@RequestBody VinylDto vinyl) throws DecodingException {
        logger.info("Request update vinyl {}", vinyl);
        return new ResponseEntity<>(this.vinylService.update(vinyl), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteVinyl(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request delete vinyl {}", id);
        return new ResponseEntity<>(this.vinylService.remove(IdEncoder.decode(id)), HttpStatus.OK);
    }

    @GetMapping("/sample/created")
    public ResponseEntity<List<VinylSampleDto>> getVinylsCreatedFrom(@RequestParam("date") LocalDate date) {
        logger.info("Request vinyls created from {}", date);
        return ResponseEntity.ok(vinylService.findVinylsCreatedFrom(LocalDateTime.of(date, LocalTime.of(0, 0))));
    }
}
