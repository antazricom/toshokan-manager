package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.BookDetailsDto;
import com.antazri.toshokan.manager.api.dto.BookDto;
import com.antazri.toshokan.manager.api.dto.BookSampleDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.BookService;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/books")
@CrossOrigin({"http://localhost:4200"})
public class BookManagementController {

    private static final Logger logger = LogManager.getLogger(BookManagementController.class);

    private final BookService bookService;

    public BookManagementController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/sample")
    public ResponseEntity<List<BookSampleDto>> getAllSampleBooks() {
        logger.info("Request get all books as samples");
        return ResponseEntity.ok(this.bookService.getAllSampleBooks());
    }

    @GetMapping("/sample/sorted")
    public ResponseEntity<Map<String, List<BookSampleDto>>> getAllSortedSampleBooks() {
        logger.info("Request get all books as samples");
        return ResponseEntity.ok(this.bookService.getAllSortedSampleBooks());
    }

    @GetMapping
    public ResponseEntity<List<BookDto>> getAllBooks() {
        logger.info("Request get all books");
        return ResponseEntity.ok(this.bookService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDetailsDto> getBookDetails(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get book details {}", id);
        BookDetailsDto book = this.bookService.getDetails(IdEncoder.decode(id))
                .orElseThrow(() -> new NoSuchElementException("No book found"));
        return ResponseEntity.ok(book);
    }

    @GetMapping("/title")
    public ResponseEntity<List<BookDto>> getBooksByTitle(@RequestBody String title) {
        logger.info("Request get books by title {}", title);
        return ResponseEntity.ok(this.bookService.getByTitle(title));
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<List<BookDto>> getBooksByCategory(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get books by category {}", id);
        return ResponseEntity.ok(this.bookService.getByCategory(IdEncoder.decode(id)));
    }

    @GetMapping("/author/{id}")
    public ResponseEntity<List<BookDto>> getBooksByAuthor(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get books by author {}", id);
        return ResponseEntity.ok(this.bookService.getByAuthor(IdEncoder.decode(id)));
    }

    @PostMapping
    public ResponseEntity<BookDto> postAddBook(@RequestBody BookDto book) throws DecodingException {
        logger.info("Request add book {}", book);
        return new ResponseEntity<>(this.bookService.create(book), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<BookDto> putUpdateBook(@RequestBody BookDto book) throws DecodingException {
        logger.info("Request update book {}", book);
        return new ResponseEntity<>(this.bookService.update(book), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteBook(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request delete book {}", id);
        return new ResponseEntity<>(this.bookService.remove(IdEncoder.decode(id)), HttpStatus.OK);
    }

    @GetMapping("/sample/created")
    public ResponseEntity<List<BookSampleDto>> getBooksCreatedFrom(@RequestParam("date") LocalDate date) {
        logger.info("Request books created from {}", date);
        return ResponseEntity.ok(this.bookService.findBooksCreatedFrom(LocalDateTime.of(date, LocalTime.of(0, 0))));
    }
}
