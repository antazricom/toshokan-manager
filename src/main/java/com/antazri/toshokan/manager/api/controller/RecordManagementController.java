package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.RecordDto;
import com.antazri.toshokan.manager.api.services.RecordService;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/records")
@CrossOrigin({"http://localhost:4200"})
public class RecordManagementController {

    private static final Logger logger = LogManager.getLogger(RecordManagementController.class);

    private final RecordService recordService;

    public RecordManagementController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/item/{id}")
    public ResponseEntity<RecordDto> getRecordByItemId(@PathVariable("id") String itemId) {
        try {
            return ResponseEntity.ok(this.recordService.getByItemId(itemId).orElse(null));
        } catch (DecodingException e) {
            logger.error("ItemId {} invalid", itemId);
        }

        return ResponseEntity.badRequest().build();
    }
}
