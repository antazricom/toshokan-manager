package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.dto.BookDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Author;
import com.antazri.toshokan.manager.model.entity.Book;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class BookMapper {

    private BookMapper() {
    }

    public static BookDto asDto(Book book) {
        List<AuthorDto> authors = book.getAuthors().stream().map(AuthorMapper::asDto).toList();

        if (book.getId() != null) {
            return new BookDto(
                    IdEncoder.encode(book.getId()),
                    book.getTitle(),
                    authors,
                    book.getPublicationYear(),
                    book.getIsbn(),
                    book.getNbPages(),
                    book.getLang().getName(),
                    book.getEdition(),
                    book.getSummary(),
                    CategoryMapper.asDto(book.getCategory())
            );
        }

        return new BookDto(
                book.getTitle(),
                authors,
                book.getPublicationYear(),
                book.getIsbn(),
                book.getNbPages(),
                book.getLang().getName(),
                book.getEdition(),
                book.getSummary(),
                CategoryMapper.asDto(book.getCategory())
        );
    }

    public static Book asEntity(BookDto dto) throws DecodingException {
        List<Author> authors = dto.getAuthors().stream().map(a -> {
            try {
                return AuthorMapper.asEntity(a);
            } catch (DecodingException e) {
                return null;
            }
        }).toList();

        if (dto.getId() != null) {
            return new Book(
                    IdEncoder.decode(dto.getId()),
                    dto.getTitle(),
                    authors,
                    dto.getPublicationYear(),
                    dto.getIsbn(),
                    dto.getNbPages(),
                    Book.Lang.valueOf(dto.getLang()),
                    dto.getEdition(),
                    dto.getSummary(),
                    CategoryMapper.asEntity(dto.getCategory())
            );
        }

        return new Book(
                dto.getTitle(),
                authors,
                dto.getPublicationYear(),
                dto.getIsbn(),
                dto.getNbPages(),
                Book.Lang.valueOf(dto.getLang()),
                dto.getEdition(),
                dto.getSummary(),
                CategoryMapper.asEntity(dto.getCategory())
        );
    }
}
