package com.antazri.toshokan.manager.api.services;

import com.antazri.toshokan.manager.api.dto.RecordDto;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;

import java.util.List;
import java.util.Optional;

public interface RecordService extends BasicService<RecordDto> {

    Optional<RecordDto> getByItemId(String itemId) throws DecodingException;

    Optional<RecordDto> getBookRecord(Integer bookId);

    Optional<RecordDto> getVinylRecord(Integer vinylId);

    List<RecordDto> getBooksExpenses();

    List<RecordDto> getVinylsExpenses();

}
