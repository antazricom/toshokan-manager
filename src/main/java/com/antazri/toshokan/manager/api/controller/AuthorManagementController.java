package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.AuthorService;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/authors")
@CrossOrigin({"http://localhost:4200"})
public class AuthorManagementController {

    private static final Logger logger = LogManager.getLogger(AuthorManagementController.class);

    private final AuthorService authorService;

    public AuthorManagementController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public ResponseEntity<List<AuthorDto>> getAllAuthors() {
        logger.info("Request all authors");
        return ResponseEntity.ok(this.authorService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDto> getAuthorDetails(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request author details {}", id);

        AuthorDto author = this.authorService.getDetails(IdEncoder.decode(id))
                .orElseThrow(() -> new NoSuchElementException("No author found"));

        return ResponseEntity.ok(author);
    }

    @PostMapping()
    public ResponseEntity<AuthorDto> postAddAuthor(@RequestBody AuthorDto author) throws DecodingException {
        logger.info("Request add author {}", author);
        return new ResponseEntity<>(this.authorService.create(author), HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<AuthorDto> putUpdateAuthor(@RequestBody AuthorDto author) throws DecodingException {
        logger.info("Request update author {}", author);
        return new ResponseEntity<>(this.authorService.update(author), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteAuthor(@PathVariable String id) throws DecodingException {
        logger.info("Request delete author {}", id);
        return new ResponseEntity<>(this.authorService.remove(IdEncoder.decode(id)), HttpStatus.OK);
    }
}
