package com.antazri.toshokan.manager.api.services;

import com.antazri.toshokan.manager.api.dto.BookDetailsDto;
import com.antazri.toshokan.manager.api.dto.BookDto;
import com.antazri.toshokan.manager.api.dto.BookProgressDto;
import com.antazri.toshokan.manager.api.dto.BookSampleDto;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface BookService extends BasicService<BookDto> {

    Optional<BookDetailsDto> getDetails(int id);

    Optional<BookDto> getByIsbn(String isbn);

    List<BookDto> getByTitle(String title);

    List<BookDto> getByPublicationYear(int year);

    List<BookDto> getByAuthor(int authorId);

    List<BookDto> getByCategory(int categoryId);

    BookProgressDto updateProgress(BookProgressDto progress) throws Exception;

    List<BookSampleDto> getAllSampleBooks();
    Map<String, List<BookSampleDto>> getAllSortedSampleBooks();

    List<BookSampleDto> findBooksCreatedFrom(LocalDateTime date);
}
