package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.VinylDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Artist;
import com.antazri.toshokan.manager.model.entity.Vinyl;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.springframework.stereotype.Component;

@Component
public final class VinylMapper {

    private VinylMapper() {
    }

    public static VinylDto asDto(Vinyl vinyl) {
        if (vinyl.getId() != null) {
            return new VinylDto(
                    IdEncoder.encode(vinyl.getId()),
                    vinyl.getTitle(),
                    ArtistMapper.asDto(vinyl.getArtist()),
                    vinyl.getPublicationYear(),
                    CategoryMapper.asDto(vinyl.getCategory())
            );
        }

        return new VinylDto(
                vinyl.getTitle(),
                ArtistMapper.asDto(vinyl.getArtist()),
                vinyl.getPublicationYear(),
                CategoryMapper.asDto(vinyl.getCategory())
        );
    }

    public static Vinyl asEntity(VinylDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Vinyl(
                    IdEncoder.decode(dto.getId()),
                    dto.getTitle(),
                    ArtistMapper.asEntity(dto.getArtist()),
                    dto.getPublicationYear(),
                    CategoryMapper.asEntity(dto.getCategory())
            );
        }

        return new Vinyl(
                dto.getTitle(),
                ArtistMapper.asEntity(dto.getArtist()),
                dto.getPublicationYear(),
                CategoryMapper.asEntity(dto.getCategory())
        );
    }
}
