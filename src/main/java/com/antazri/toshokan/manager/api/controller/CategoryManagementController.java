package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.CategoryDto;
import com.antazri.toshokan.manager.api.services.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category")
@CrossOrigin({"http://localhost:4200"})
public class CategoryManagementController {

    private final CategoryService categoryService;

    public CategoryManagementController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<List<CategoryDto>> getAllCategories() {
        return ResponseEntity.ok(this.categoryService.getAll());
    }

    @GetMapping("/book")
    public ResponseEntity<List<CategoryDto>> getAllBookCategories() {
        return ResponseEntity.ok(this.categoryService.getByType("book"));
    }

    @GetMapping("/vinyl")
    public ResponseEntity<List<CategoryDto>> getAllVinylCategories() {
        return ResponseEntity.ok(this.categoryService.getByType("vinyl"));
    }
}
