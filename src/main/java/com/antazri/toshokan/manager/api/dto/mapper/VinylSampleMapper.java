package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.VinylSampleDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Vinyl;

public class VinylSampleMapper {

    private VinylSampleMapper() {
    }

    public static VinylSampleDto asDto(Vinyl vinyl) {
        if (vinyl.getId() != null) {
            return new VinylSampleDto(
                    IdEncoder.encode(vinyl.getId()),
                    vinyl.getTitle(),
                    ArtistMapper.asDto(vinyl.getArtist()),
                    vinyl.getCategory().getName(),
                    vinyl.getCreatedAt()
            );
        }

        return new VinylSampleDto(
                vinyl.getTitle(),
                ArtistMapper.asDto(vinyl.getArtist()),
                vinyl.getCategory().getName(),
                vinyl.getCreatedAt()
        );
    }
}
