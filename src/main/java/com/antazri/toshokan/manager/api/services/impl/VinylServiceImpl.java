package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.ArtistDto;
import com.antazri.toshokan.manager.api.dto.CategoryDto;
import com.antazri.toshokan.manager.api.dto.VinylDto;
import com.antazri.toshokan.manager.api.dto.VinylSampleDto;
import com.antazri.toshokan.manager.api.dto.mapper.VinylMapper;
import com.antazri.toshokan.manager.api.dto.mapper.VinylSampleMapper;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.VinylService;
import com.antazri.toshokan.manager.data.repositories.ArtistDao;
import com.antazri.toshokan.manager.data.repositories.CategoryDao;
import com.antazri.toshokan.manager.data.repositories.VinylDao;
import com.antazri.toshokan.manager.model.entity.Artist;
import com.antazri.toshokan.manager.model.entity.Category;
import com.antazri.toshokan.manager.model.entity.Vinyl;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

@Service
@Qualifier("vinylService")
public class VinylServiceImpl implements VinylService {

    private static final Logger logger = LogManager.getLogger(VinylServiceImpl.class);

    private final VinylDao vinylDao;
    private final ArtistDao artistDao;
    private final CategoryDao categoryDao;

    public VinylServiceImpl(VinylDao vinylDao, ArtistDao artistDao, CategoryDao categoryDao) {
        this.vinylDao = vinylDao;
        this.artistDao = artistDao;
        this.categoryDao = categoryDao;
    }

    @Override
    public Optional<VinylDto> getDetails(int id) {
        final Optional<Vinyl> vinyl = vinylDao.findById(id);

        if (vinyl.isEmpty()) {
            logger.info("No Vinyl found with id {}", id);
            return Optional.empty();
        }

        return vinyl.map(VinylMapper::asDto);
    }

    @Override
    public List<VinylDto> getByTitle(String title) {
        final Collection<Vinyl> vinylsDb = vinylDao.findByTitle(title.trim());

        if (vinylsDb.isEmpty()) {
            logger.info("No vinyl found with title {}", title);
            return Collections.emptyList();
        }

        return vinylsDb.stream().map(VinylMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getByArtist(int artistId) {
        Optional<Artist> artistOpt = artistDao.findById(artistId);

        if (artistOpt.isEmpty()) {
            logger.error("No artist found with id {}", artistId);
            return Collections.emptyList();
        }

        return vinylDao.findByArtist(artistOpt.get()).stream().map(VinylMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getByPublicationYear(int year) {
        return vinylDao.findByPublicationYear(year).stream().map(VinylMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getByCategory(int categoryId) {
        Optional<Category> categoryOpt = categoryDao.findById(categoryId);

        if (categoryOpt.isEmpty()) {
            logger.error("No category found with id {}", categoryId);
            return Collections.emptyList();
        }

        return vinylDao.findByCategory(categoryOpt.get()).stream().map(VinylMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<VinylDto> getAll() {
        return vinylDao.findAll().stream()
                .map(VinylMapper::asDto)
                .collect(Collectors.toList());
    }

    @Override
    public VinylDto create(VinylDto vinyl) throws DecodingException {
        if (!validCreateBodyRequest(vinyl)) {
            logger.error("Vinyl to add is not valid");
            throw new BadRequestException("Vinyl to add is not valid");
        }

        checkArtist(vinyl.getArtist());
        checkCategory(vinyl.getCategory());

        logger.info("Add Vinyl {}", vinyl);
        Vinyl vinylDb = VinylMapper.asEntity(vinyl);

        return VinylMapper.asDto(vinylDao.save(vinylDb));
    }

    private boolean validCreateBodyRequest(VinylDto vinyl) {
        return Stream.of(vinyl.getTitle(), vinyl.getArtist(), vinyl.getCategory(), vinyl.getPublicationYear())
                .noneMatch(Objects::isNull)
                && !vinyl.getTitle().isBlank()
                && (vinyl.getPublicationYear() > 0 && vinyl.getPublicationYear() <= LocalDate.now().getYear());
    }

    @Override
    public VinylDto update(VinylDto vinyl) throws DecodingException {
        if (!this.vinylDao.existsById(IdEncoder.decode(vinyl.getId()))) {
            logger.error("No vinyl found with id {}", vinyl.getId());
            throw new IdNotValidException("Vinyl is not valid");
        }

        if (!validUpdateBodyRequest(vinyl)) {
            logger.error("Vinyl to update is not valid");
            throw new BadRequestException("Vinyl to update is not valid");
        }

        checkArtist(vinyl.getArtist());
        checkCategory(vinyl.getCategory());

        logger.info("Update Vinyl {}", vinyl);
        Vinyl vinylDb = VinylMapper.asEntity(vinyl);

        return VinylMapper.asDto(vinylDao.save(vinylDb));
    }

    private boolean validUpdateBodyRequest(VinylDto vinyl) {
        return Stream.of(vinyl.getId(), vinyl.getTitle(), vinyl.getArtist(), vinyl.getCategory(), vinyl.getPublicationYear())
                .noneMatch(Objects::isNull)
                && !vinyl.getTitle().isBlank()
                && (vinyl.getPublicationYear() > 0 && vinyl.getPublicationYear() <= LocalDate.now().getYear());
    }

    private void checkCategory(CategoryDto category) throws DecodingException {
        if (category != null && !this.categoryDao.existsById(IdEncoder.decode(category.getId()))) {
            logger.error("No category found with id {}", category);
            throw new IdNotValidException("Category is not valid");
        }
    }

    private void checkArtist(ArtistDto artist) throws DecodingException {
        if (artist != null && !this.artistDao.existsById(IdEncoder.decode(artist.getId()))) {
            logger.error("No artist found with id {}", artist);
            throw new IdNotValidException("Artist is not valid");
        }
    }


    @Override
    public boolean remove(int id) {
        final Optional<Vinyl> vinyl = vinylDao.findById(id);

        if (vinyl.isEmpty()) {
            logger.error("No vinyl find with id {}", id);
            throw new IdNotValidException("No vinyl found");
        }

        logger.info("Remove Vinyl {}", vinyl.get());
        vinylDao.delete(vinyl.get());
        return true;
    }

    @Override
    public List<VinylSampleDto> getAllVinylSamples() {
        return this.vinylDao.findAll().stream()
                .map(VinylSampleMapper::asDto)
                .sorted(Comparator.comparing(VinylSampleDto::getTitle))
                .toList();
    }

    @Override
    public Map<String, List<VinylSampleDto>> getAllSortedVinylSamples() {
        return this.vinylDao.findAll().stream()
                .map(VinylSampleMapper::asDto)
                .collect(groupingBy(v -> String.valueOf(v.getTitle().toLowerCase().charAt(0))));
    }

    @Override
    public List<VinylSampleDto> findVinylsCreatedFrom(LocalDateTime date) {
        return this.vinylDao.findByCreatedAtAfter(date)
                .stream().map(VinylSampleMapper::asDto)
                .sorted(Comparator.comparing(VinylSampleDto::getCreatedAt))
                .toList();
    }
}
