package com.antazri.toshokan.manager.api.services;


import com.antazri.toshokan.manager.api.dto.AuthorDto;

import java.util.List;
import java.util.Optional;

public interface AuthorService extends BasicService<AuthorDto> {

    Optional<AuthorDto> getDetails(int id);

    List<AuthorDto> searchByName(String name);

}
