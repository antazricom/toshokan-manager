package com.antazri.toshokan.manager.api.filter;

import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebFilter(urlPatterns = "/*")
public class RequestParameterDecoderFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Map<String, String[]> parameterMap = request.getParameterMap();

        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            if (entry.getKey().toLowerCase().endsWith("id")) {
                try {
                    request.getParameterMap().replace(entry.getKey(), new String[]{String.valueOf(IdEncoder.decode(entry.getValue()[0]))});
                } catch (DecodingException e) {
                    throw new ServletException("Error while decoding id in request uri");
                }
            }
        }

        filterChain.doFilter(request, response);
    }
}
