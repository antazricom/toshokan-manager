package com.antazri.toshokan.manager.api.dto;

import java.io.Serializable;
import java.util.Objects;

public class VinylDto implements Serializable {

    private static final long serialVersionUID = -4976044621906178234L;
	private String id;
    private String title;
    private ArtistDto artist;
    private int publicationYear;
    private CategoryDto category;

    public VinylDto() {
    }

    public VinylDto(String title, ArtistDto artist, int publicationYear, CategoryDto category) {
        this.title = title;
        this.artist = artist;
        this.publicationYear = publicationYear;
        this.category = category;
    }

    public VinylDto(String id, String title, ArtistDto artist, int publicationYear, CategoryDto category) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.publicationYear = publicationYear;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArtistDto getArtist() {
        return artist;
    }

    public void setArtist(ArtistDto artist) {
        this.artist = artist;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VinylDto vinylDto = (VinylDto) o;
        return publicationYear == vinylDto.publicationYear
                && Objects.equals(id, vinylDto.id)
                && Objects.equals(title, vinylDto.title)
                && Objects.equals(artist, vinylDto.artist)
                && Objects.equals(category, vinylDto.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, artist, publicationYear, category);
    }

    @Override
    public String toString() {
        return "VinylDto{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", artist=" + artist +
                ", publicationYear=" + publicationYear +
                ", category=" + category +
                '}';
    }
}
