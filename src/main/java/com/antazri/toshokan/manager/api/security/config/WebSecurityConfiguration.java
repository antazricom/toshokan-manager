package com.antazri.toshokan.manager.api.security.config;

import com.antazri.toshokan.manager.api.filter.AuthorizationFilter;
import com.antazri.toshokan.manager.api.security.auth.BasicPasswordEncoder;
import com.antazri.toshokan.manager.api.security.auth.service.BasicUserService;
import com.antazri.toshokan.manager.api.security.token.JwtHMAC512Provider;
import com.antazri.toshokan.manager.api.security.token.JwtProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration {

    private static final String CONTEXT = "/api";
    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String PUT = "PUT";
    private static final String DELETE = "DELETE";

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .addFilterAfter(new AuthorizationFilter(jwtProvider()), UsernamePasswordAuthenticationFilter.class)
                .securityMatcher(CONTEXT +"/**")
                .authorizeHttpRequests(authorize -> authorize
//                        .requestMatchers(new AntPathRequestMatcher(CONTEXT + "/", GET)).permitAll()
//                        .requestMatchers(new AntPathRequestMatcher(CONTEXT + "/auth", POST)).permitAll()
//                        .anyRequest().authenticated()
                                .anyRequest().permitAll()
                );

        return http.build();
    }

    /*@Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        // Commented as no front is deployed yet
        configuration.setAllowedOrigins(List.of("http://localhost:4200"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST", "PUT", "DELETE"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/api/**", configuration);
        return source;
    }*/

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BasicPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new BasicUserService();
    }

    @Bean
    public JwtProvider jwtProvider() {
        return new JwtHMAC512Provider();
    }
}
