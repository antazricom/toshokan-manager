package com.antazri.toshokan.manager.api.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class BookSampleDto implements Serializable {

    private static final long serialVersionUID = 8567146308572169868L;
	private String id;
    private String title;
    private List<AuthorDto> authors;
    private String category;
    private String progressStatus;
    private LocalDateTime createdAt;

    public BookSampleDto(
            String title,
            List<AuthorDto> authors,
            String category,
            String progressStatus,
            LocalDateTime createdAt
    ) {
        this.title = title;
        this.authors = authors;
        this.category = category;
        this.progressStatus = progressStatus;
        this.createdAt = createdAt;
    }

    public BookSampleDto(String id,
                         String title,
                         List<AuthorDto> authors,
                         String category,
                         String progressStatus,
                         LocalDateTime createdAt
    ) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.category = category;
        this.progressStatus = progressStatus;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AuthorDto> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorDto> authors) {
        this.authors = authors;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(String progressStatus) {
        this.progressStatus = progressStatus;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookSampleDto that = (BookSampleDto) o;
        return Objects.equals(id, that.id)
                && Objects.equals(title, that.title)
                && Objects.equals(authors, that.authors)
                && Objects.equals(createdAt, that.createdAt)
                && Objects.equals(category, that.category)
                && Objects.equals(progressStatus, that.progressStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, authors, createdAt, category, progressStatus);
    }

    @Override
    public String toString() {
        return "BookSampleDto{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", authors=" + authors +
                ", createdAt=" + createdAt +
                ", category='" + category + '\'' +
                ", progressStatus='" + progressStatus + '\'' +
                '}';
    }
}
