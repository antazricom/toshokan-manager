package com.antazri.toshokan.manager.api.services;

import com.antazri.toshokan.manager.api.dto.VinylDto;
import com.antazri.toshokan.manager.api.dto.VinylSampleDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface VinylService extends BasicService<VinylDto> {

    Optional<VinylDto> getDetails(int id);

    List<VinylDto> getByArtist(int artistId);

    List<VinylDto> getByPublicationYear(int year);

    List<VinylDto> getByCategory(int categoryId);

    List<VinylDto> getByTitle(String title);

    List<VinylSampleDto> getAllVinylSamples();

    Map<String, List<VinylSampleDto>> getAllSortedVinylSamples();

    List<VinylSampleDto> findVinylsCreatedFrom(LocalDateTime date);
}
