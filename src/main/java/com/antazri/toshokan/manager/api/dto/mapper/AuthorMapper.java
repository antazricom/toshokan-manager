package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Author;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.springframework.stereotype.Component;

@Component
public final class AuthorMapper {

    private AuthorMapper() {
    }

    public static AuthorDto asDto(Author author) {
        if (author.getId() != null) {
            return new AuthorDto(
                    IdEncoder.encode(author.getId()),
                    author.getFirstname(),
                    author.getMiddlename() != null && !author.getMiddlename().isBlank() ? author.getMiddlename() : "",
                    author.getLastname()
            );
        }

        return new AuthorDto(
                author.getFirstname(),
                author.getMiddlename() != null && !author.getMiddlename().isBlank() ? author.getMiddlename() : "",
                author.getLastname()
        );
    }

    public static Author asEntity(AuthorDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Author(
                    IdEncoder.decode(dto.getId()),
                    dto.getFirstname(),
                    dto.getMiddlename() != null && !dto.getMiddlename().isBlank() ? dto.getMiddlename() : null,
                    dto.getLastname()
            );
        }

        return new Author(
                dto.getFirstname(),
                dto.getMiddlename() != null && !dto.getMiddlename().isBlank() ? dto.getMiddlename() : null,
                dto.getLastname()
        );
    }
}
