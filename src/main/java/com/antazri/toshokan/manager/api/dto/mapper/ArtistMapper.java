package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.ArtistDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Artist;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.springframework.stereotype.Component;

@Component
public final class ArtistMapper {

    private ArtistMapper() {
    }

    public static ArtistDto asDto(Artist artist) {
        if (artist.getId() != null) {
            return new ArtistDto(
                    IdEncoder.encode(artist.getId()),
                    artist.getName()
            );
        }

        return new ArtistDto(artist.getName());
    }

    public static Artist asEntity(ArtistDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Artist(
                    IdEncoder.decode(dto.getId()),
                    dto.getName()
            );
        }

        return new Artist(dto.getName());
    }
}
