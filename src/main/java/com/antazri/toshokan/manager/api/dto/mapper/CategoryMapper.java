package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.CategoryDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Category;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.springframework.stereotype.Component;

@Component
public final class CategoryMapper {

    private CategoryMapper() {
    }

    public static CategoryDto asDto(Category category) {
        if (category.getId() != null) {
            return new CategoryDto(
                    IdEncoder.encode(category.getId()),
                    category.getName(),
                    category.getType()
            );
        }

        return new CategoryDto(category.getName(), category.getType());
    }

    public static Category asEntity(CategoryDto dto) throws DecodingException {
        if (dto.getId() != null) {
            return new Category(
                    IdEncoder.decode(dto.getId()),
                    dto.getName(),
                    dto.getType()
            );
        }

        return new Category(dto.getName(), dto.getType());
    }
}
