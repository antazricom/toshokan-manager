package com.antazri.toshokan.manager.api.services;

import com.antazri.toshokan.manager.technical.exceptions.DecodingException;

import java.util.List;

public interface BasicService<T> {

    List<T> getAll();

    T create(T entity) throws DecodingException;

    T update(T entity) throws DecodingException;

    boolean remove(int id);
}
