package com.antazri.toshokan.manager.api.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class RecordDto implements Serializable {

    private static final long serialVersionUID = 8123569345525352903L;
	private String id;
    private LocalDateTime date;
    private Integer number;
    private String location;
    private Double expense;
    private Double income;
    private String type;
    private String itemId;

    public RecordDto(LocalDateTime date, Integer number, String location, Double expense, Double income, String type, String itemId) {
        this.date = date;
        this.number = number;
        this.location = location;
        this.expense = expense;
        this.income = income;
        this.type = type;
        this.itemId = itemId;
    }

    public RecordDto(String id, LocalDateTime date, Integer number, String location, Double expense, Double income, String type, String itemId) {
        this(date, number, location, expense, income, type, itemId);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getExpense() {
        return expense;
    }

    public void setExpense(Double expense) {
        this.expense = expense;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
