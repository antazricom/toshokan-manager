package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.dto.BookSampleDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Book;

import java.util.List;

public class BookSampleMapper {

    private BookSampleMapper() {
    }

    public static BookSampleDto asDto(Book book) {
        List<AuthorDto> authors = book.getAuthors().stream().map(AuthorMapper::asDto).toList();

        if (book.getId() != null) {
            return new BookSampleDto(
                    IdEncoder.encode(book.getId()),
                    book.getTitle(),
                    authors,
                    book.getCategory().getName(),
                    book.getBookProgress().getStatus().getName(),
                    book.getCreatedAt()
            );
        }

        return new BookSampleDto(
                book.getTitle(),
                authors,
                book.getCategory().getName(),
                book.getBookProgress().getStatus().getName(),
                book.getCreatedAt()
        );
    }
}
