package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.AuthorDto;
import com.antazri.toshokan.manager.api.dto.mapper.AuthorMapper;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.AuthorService;
import com.antazri.toshokan.manager.data.repositories.AuthorDao;
import com.antazri.toshokan.manager.model.entity.Author;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("authorService")
public class AuthorServiceImpl implements AuthorService {

    private static final Logger logger = LogManager.getLogger(AuthorServiceImpl.class);

    private final AuthorDao authorDao;

    public AuthorServiceImpl(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    @Override
    public Optional<AuthorDto> getDetails(int id) {
        final Optional<Author> author = authorDao.findById(id);


        return author.map(AuthorMapper::asDto);
    }

    @Override
    public List<AuthorDto> searchByName(String name) {
        return authorDao.findByName(name).stream().map(AuthorMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public List<AuthorDto> getAll() {
        return authorDao.findAll().stream().map(AuthorMapper::asDto).collect(Collectors.toList());
    }

    @Override
    public AuthorDto create(AuthorDto author) throws DecodingException {
        if (!validCreateBodyRequest(author)) {
            logger.error("Author {} from request is not valid", author);
            throw new BadRequestException("Author from request is not valid");
        }

        logger.info("Add {}", author);
        Author authorDb = AuthorMapper.asEntity(author);

        return AuthorMapper.asDto(authorDao.save(authorDb));
    }

    private boolean validCreateBodyRequest(AuthorDto author) {
        return Stream.of(author.getFirstname(), author.getLastname()).noneMatch(Objects::isNull)
                && Stream.of(author.getFirstname(), author.getLastname()).noneMatch(String::isBlank);
    }

    @Override
    public AuthorDto update(AuthorDto author) throws DecodingException {
        if (!authorDao.existsById(IdEncoder.decode(author.getId()))) {
            logger.error("Update attempt on unknown entity : {}", author);
            throw new IdNotValidException("Author to update is unknown");
        }

        if (!validUpdateBodyRequest(author)) {
            logger.error("Update for Author {} not valid", author);
            throw new BadRequestException("Author update not valid");
        }

        logger.info("Update {}", author);
        Author dbAuthor = AuthorMapper.asEntity(author);

        return AuthorMapper.asDto(dbAuthor);
    }

    private boolean validUpdateBodyRequest(AuthorDto author) {
        return Stream.of(author.getId(), author.getFirstname(), author.getLastname()).noneMatch(Objects::isNull)
                && Stream.of(author.getId(), author.getFirstname(), author.getLastname()).noneMatch(String::isBlank);
    }

    @Override
    public boolean remove(int id) {
        Optional<Author> author = authorDao.findById(id);
        if (author.isEmpty()) {
            logger.error("Author not found with id: {}", id);
            throw new IdNotValidException("Author to remove not found");
        }

        logger.info("Remove Author {}", author.get());
        authorDao.delete(author.get());
        return true;
    }
}
