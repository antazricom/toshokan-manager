package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.RecordDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Record;
import com.antazri.toshokan.manager.model.entity.Item;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.springframework.stereotype.Component;

@Component
public class RecordMapper {

    private RecordMapper() {
    }

    public static RecordDto asDto(Record record) {
        if (record.getId() != null) {
            return new RecordDto(
                    IdEncoder.encode(record.getId()),
                    record.getDate(),
                    record.getNumber(),
                    record.getLocation(),
                    record.getExpense(),
                    record.getIncome(),
                    record.getType().name(),
                    IdEncoder.encode(record.getItem().getId())
            );
        }

        return new RecordDto(
                record.getDate(),
                record.getNumber(),
                record.getLocation(),
                record.getExpense(),
                record.getIncome(),
                record.getType().name(),
                IdEncoder.encode(record.getItem().getId())
        );
    }

    public static Record asEntity(RecordDto dto) throws DecodingException {
        Item item = new Item();
        item.setId(IdEncoder.decode(dto.getItemId()));
        if (dto.getId() != null) {
            return new Record(
                    IdEncoder.decode(dto.getId()),
                    dto.getDate(),
                    item,
                    Record.Type.valueOf(dto.getType()),
                    dto.getNumber(),
                    dto.getLocation(),
                    dto.getExpense(),
                    dto.getIncome()
            );
        }

        return new Record(
                dto.getDate(),
                item,
                Record.Type.valueOf(dto.getType()),
                dto.getNumber(),
                dto.getLocation(),
                dto.getExpense(),
                dto.getIncome()
        );
    }
}
