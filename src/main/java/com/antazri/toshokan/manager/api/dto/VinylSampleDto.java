package com.antazri.toshokan.manager.api.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class VinylSampleDto implements Serializable {

    private static final long serialVersionUID = -133140086683353146L;
	private String id;
    private String title;
    private ArtistDto artist;
    private String category;
    private LocalDateTime createdAt;

    public VinylSampleDto(String title, ArtistDto artist, String category, LocalDateTime createdAt) {
        this.title = title;
        this.artist = artist;
        this.category = category;
        this.createdAt = createdAt;
    }

    public VinylSampleDto(String id, String title, ArtistDto artist, String category, LocalDateTime createdAt) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.category = category;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArtistDto getArtist() {
        return artist;
    }

    public void setArtist(ArtistDto artist) {
        this.artist = artist;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VinylSampleDto that = (VinylSampleDto) o;
        return Objects.equals(id, that.id)
                && Objects.equals(title, that.title)
                && Objects.equals(artist, that.artist)
                && Objects.equals(category, that.category)
                && Objects.equals(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, artist, category, createdAt);
    }

    @Override
    public String toString() {
        return "VinylSampleDto{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", artist=" + artist +
                ", category='" + category + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
