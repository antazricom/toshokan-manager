package com.antazri.toshokan.manager.api.dto;

import java.io.Serializable;

public class BookProgressDto implements Serializable {

    private static final long serialVersionUID = -1843364206875943310L;
	private String id;
    private int nbReadPages;
    private String status;
    private String bookId;

    public BookProgressDto(int nbReadPages, String status, String bookId) {
        this.nbReadPages = nbReadPages;
        this.status = status;
        this.bookId = bookId;
    }

    public BookProgressDto(String id, int nbReadPages, String status, String bookId) {
        this.id = id;
        this.nbReadPages = nbReadPages;
        this.status = status;
        this.bookId = bookId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNbReadPages() {
        return nbReadPages;
    }

    public void setNbReadPages(int nbReadPages) {
        this.nbReadPages = nbReadPages;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }
}
