package com.antazri.toshokan.manager.api.dto.mapper;

import com.antazri.toshokan.manager.api.dto.BookProgressDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.model.entity.Book;
import com.antazri.toshokan.manager.model.entity.BookProgress;
import com.antazri.toshokan.manager.model.utils.BookProgressStatus;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;

public class BookProgressMapper {

    public static BookProgressDto asDto(BookProgress progress) {
        if (progress.getId() != null) {
            return new BookProgressDto(
                    IdEncoder.encode(progress.getId()),
                    progress.getNbReadPages(),
                    progress.getStatus().getName(),
                    IdEncoder.encode(progress.getBook().getId())
            );
        }

        return new BookProgressDto(
                progress.getNbReadPages(),
                progress.getStatus().getName(),
                IdEncoder.encode(progress.getBook().getId())
        );
    }

    public static BookProgress asEntity(BookProgressDto progressDto) throws DecodingException {
        Book book = new Book();
        book.setId(IdEncoder.decode(progressDto.getBookId()));

        if (progressDto.getId() != null) {
            return new BookProgress(
                    IdEncoder.decode(progressDto.getId()),
                    progressDto.getNbReadPages(),
                    BookProgressStatus.valueOf(progressDto.getStatus()),
                    book
            );
        }

        return new BookProgress(
                progressDto.getNbReadPages(),
                BookProgressStatus.valueOf(progressDto.getStatus()),
                book
        );
    }
}
