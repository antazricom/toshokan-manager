package com.antazri.toshokan.manager.api.security.auth.service;

import com.antazri.toshokan.manager.api.security.auth.BasicPasswordEncoder;
import com.antazri.toshokan.manager.api.security.auth.LoginVerifier;
import com.antazri.toshokan.manager.api.security.auth.user.IdentifiedUserDetails;
import com.antazri.toshokan.manager.api.security.token.JwtHMAC512Provider;
import com.antazri.toshokan.manager.api.security.token.JwtProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BasicAuthenticationService implements AuthenticationService {

    private static final Logger logger = LogManager.getLogger(BasicAuthenticationService.class);

    private final JwtProvider jwtProvider;
    private final UserDetailsService userDetailsService;

    public BasicAuthenticationService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
        this.jwtProvider = new JwtHMAC512Provider();
    }

    @Override
    public String auth(String login, String password) {
        logger.info("Authentication attempt with login {}", login);

        LoginVerifier.check(login);

        IdentifiedUserDetails userDetails = (IdentifiedUserDetails) userDetailsService.loadUserByUsername(login);

        if (!verifyPassword(userDetails, password)) {
            logger.error("Authentication denied for login {}", userDetails.getUsername());
            throw new AccessDeniedException("Access denied : email and password do not match");
        }

        return jwtProvider.generate(
                Map.of(
                        "login", userDetails.getUsername(),
                        "role", List.of(userDetails.getAuthorities()).get(0).toString()
                )
        );
    }

    private boolean verifyPassword(UserDetails userDetails, String clearPassword) {
        return new BasicPasswordEncoder().matches(clearPassword, userDetails.getPassword());
    }
}
