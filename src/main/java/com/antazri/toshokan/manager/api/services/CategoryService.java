package com.antazri.toshokan.manager.api.services;

import com.antazri.toshokan.manager.api.dto.CategoryDto;

import java.util.List;
import java.util.Optional;

public interface CategoryService extends BasicService<CategoryDto> {

    Optional<CategoryDto> getDetails(int id);

    List<CategoryDto> getByType(String type);

}
