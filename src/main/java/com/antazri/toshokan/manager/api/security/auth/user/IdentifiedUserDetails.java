package com.antazri.toshokan.manager.api.security.auth.user;

import org.springframework.security.core.userdetails.UserDetails;

public interface IdentifiedUserDetails extends UserDetails {

    Integer getId();
}
