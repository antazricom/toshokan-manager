package com.antazri.toshokan.manager.api.controller;

import com.antazri.toshokan.manager.api.dto.ArtistDto;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.ArtistService;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/artists")
public class ArtistManagementController {

    private static final Logger logger = LogManager.getLogger(ArtistManagementController.class);

    private final ArtistService artistService;

    public ArtistManagementController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ArtistDto> getArtistDetails(@PathVariable("id") String id) throws DecodingException {
        logger.info("Request get artist details {}", id);
        ArtistDto artist = this.artistService.getDetails(IdEncoder.decode(id))
                .orElseThrow(() -> new NoSuchElementException("No artist found"));
        return ResponseEntity.ok(artist);
    }

    @GetMapping
    public ResponseEntity<List<ArtistDto>> getAllArtists() {
        logger.info("Request get all artist");
        return ResponseEntity.ok(this.artistService.getAll());
    }

    @PostMapping()
    public ResponseEntity<ArtistDto> postAddArtist(@RequestBody ArtistDto artist) throws DecodingException {
        logger.info("Request add artist {}", artist);
        return new ResponseEntity<>(this.artistService.create(artist), HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<ArtistDto> putUpdateArtist(@RequestBody ArtistDto artist) throws DecodingException {
        logger.info("Request update artist {}", artist);
        return new ResponseEntity<>(this.artistService.update(artist), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteArtist(@PathVariable String id) throws DecodingException {
        logger.info("Request delete artist {}", id);
        return new ResponseEntity<>(this.artistService.remove(IdEncoder.decode(id)), HttpStatus.OK);
    }
}
