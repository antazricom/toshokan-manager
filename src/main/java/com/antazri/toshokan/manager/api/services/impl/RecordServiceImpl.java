package com.antazri.toshokan.manager.api.services.impl;

import com.antazri.toshokan.manager.api.dto.RecordDto;
import com.antazri.toshokan.manager.api.dto.mapper.RecordMapper;
import com.antazri.toshokan.manager.api.security.IdEncoder;
import com.antazri.toshokan.manager.api.services.RecordService;
import com.antazri.toshokan.manager.data.repositories.ItemDao;
import com.antazri.toshokan.manager.data.repositories.RecordDao;
import com.antazri.toshokan.manager.model.entity.Item;
import com.antazri.toshokan.manager.model.entity.Record;
import com.antazri.toshokan.manager.technical.exceptions.BadRequestException;
import com.antazri.toshokan.manager.technical.exceptions.DecodingException;
import com.antazri.toshokan.manager.technical.exceptions.IdNotValidException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Service("recordServiceImpl")
public class RecordServiceImpl implements RecordService {

    private static final Logger logger = LogManager.getLogger(RecordServiceImpl.class);

    private final RecordDao recordDao;
    private final ItemDao itemDao;

    public RecordServiceImpl(RecordDao recordDao, ItemDao itemDao) {
        this.recordDao = recordDao;
        this.itemDao = itemDao;
    }

    @Override
    public List<RecordDto> getAll() {
        return recordDao.findAll().stream().map(RecordMapper::asDto).toList();
    }

    @Override
    public RecordDto create(RecordDto entity) throws DecodingException {
        if (!isvalidCreateRequest(entity)) {
            logger.error("Request to add new record is not valid");
            throw new BadRequestException("Request to add new record is not valid");
        }

        checkItem(entity.getItemId());

        Record record = this.recordDao.save(RecordMapper.asEntity(entity));
        return RecordMapper.asDto(record);
    }

    private boolean isvalidCreateRequest(RecordDto recordDto) {
        return Stream.of(recordDto.getDate(), recordDto.getType(), recordDto.getNumber(),
                recordDto.getItemId(), recordDto.getExpense(), recordDto.getIncome()).noneMatch(Objects::isNull)
                && recordDto.getExpense() >= 0.0
                && recordDto.getIncome() >= 0.0
                && recordDto.getNumber() >= 0
                && !recordDto.getDate().isAfter(LocalDateTime.now());
    }

    @Override
    public RecordDto update(RecordDto entity) throws DecodingException {
        if (!this.recordDao.existsById(IdEncoder.decode(entity.getId()))) {
            logger.error("Record not found with id {} ({})", entity.getId(), IdEncoder.decode(entity.getId()));
            throw new IdNotValidException("Record id not valid or not found");
        }

        if (!isValidUpdateRequest(entity)) {
            logger.error("Request to update record is not valid");
            throw new BadRequestException("Request to update record is not valid");
        }

        checkItem(entity.getItemId());

        Record record = this.recordDao.save(RecordMapper.asEntity(entity));
        return RecordMapper.asDto(record);
    }

    private boolean isValidUpdateRequest(RecordDto recordDto) {
        return Stream.of(recordDto.getId(), recordDto.getDate(), recordDto.getType(), recordDto.getNumber(),
                recordDto.getItemId(), recordDto.getExpense(), recordDto.getIncome()).noneMatch(Objects::isNull)
                && recordDto.getExpense() >= 0.0
                && recordDto.getIncome() >= 0.0
                && recordDto.getNumber() >= 0
                && !recordDto.getDate().isAfter(LocalDateTime.now());
    }

    private void checkItem(String id) throws DecodingException {
        if (!this.itemDao.existsById(IdEncoder.decode(id))) {
            logger.error("Item id associated with record not found with id {} ({})", id, IdEncoder.decode(id));
            throw new IdNotValidException("Item id associated with record not valid or not found");
        }
    }

    @Override
    public boolean remove(int id) {
        Optional<Record> record = this.recordDao.findById(id);

        if (record.isEmpty()) {
            logger.error("Item id associated with record not found with id {}", id);
            throw new IdNotValidException("Item id associated with record not valid or not found");
        }

        this.recordDao.delete(record.get());
        return true;
    }

    @Override
    public Optional<RecordDto> getBookRecord(Integer bookId) {
        Item item = new Item(bookId);
        return this.recordDao.findByItem(item).map(RecordMapper::asDto);
    }

    @Override
    public Optional<RecordDto> getVinylRecord(Integer vinylId) {
        Item item = new Item(vinylId);
        return this.recordDao.findByItem(item).map(RecordMapper::asDto);
    }

    @Override
    public List<RecordDto> getBooksExpenses() {
        return this.recordDao.findAllByType(Record.Type.BOOK).stream()
                .map(RecordMapper::asDto)
                .toList();
    }

    @Override
    public List<RecordDto> getVinylsExpenses() {
        return this.recordDao.findAllByType(Record.Type.VINYL).stream()
                .map(RecordMapper::asDto)
                .toList();
    }

    @Override
    public Optional<RecordDto> getByItemId(String itemId) throws DecodingException {
        return this.recordDao.findByItemId(IdEncoder.decode(itemId)).map(RecordMapper::asDto);
    }
}
