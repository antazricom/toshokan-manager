package com.antazri.toshokan.manager.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class BookDetailsDto implements Serializable {

    private static final long serialVersionUID = -3841613048493684157L;
	private String id;
    private String title;
    private List<AuthorDto> authors;
    private int publicationYear;
    private String isbn;
    private int nbPages;
    private String lang;
    private String edition;
    private String summary;
    private CategoryDto category;
    private BookProgressDto bookProgress;

    public BookDetailsDto() {
    }

    public BookDetailsDto(
            String title,
            List<AuthorDto> authors,
            int publicationYear,
            String isbn,
            int nbPages,
            String lang,
            String edition,
            String summary,
            CategoryDto category,
            BookProgressDto bookProgress
    ) {
        this.title = title;
        this.authors = authors;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.nbPages = nbPages;
        this.lang = lang;
        this.edition = edition;
        this.summary = summary;
        this.category = category;
        this.bookProgress = bookProgress;
    }

    public BookDetailsDto(
            String id,
            String title,
            List<AuthorDto> authors,
            int publicationYear,
            String isbn,
            int nbPages,
            String lang,
            String edition,
            String summary,
            CategoryDto category,
            BookProgressDto bookProgress
    ) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.nbPages = nbPages;
        this.lang = lang;
        this.edition = edition;
        this.summary = summary;
        this.category = category;
        this.bookProgress = bookProgress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AuthorDto> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorDto> authors) {
        this.authors = authors;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getNbPages() {
        return nbPages;
    }

    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public BookProgressDto getBookProgress() {
        return bookProgress;
    }

    public void setBookProgress(BookProgressDto bookProgress) {
        this.bookProgress = bookProgress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDetailsDto bookDto = (BookDetailsDto) o;
        return publicationYear == bookDto.publicationYear
                && nbPages == bookDto.nbPages
                && Objects.equals(id, bookDto.id)
                && Objects.equals(title, bookDto.title)
                && Objects.equals(authors, bookDto.authors)
                && Objects.equals(isbn, bookDto.isbn)
                && Objects.equals(lang, bookDto.lang)
                && Objects.equals(edition, bookDto.edition)
                && Objects.equals(summary, bookDto.summary)
                && Objects.equals(category, bookDto.category)
                && Objects.equals(bookProgress, bookDto.bookProgress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                title,
                authors,
                publicationYear,
                isbn,
                nbPages,
                lang,
                edition,
                summary,
                category,
                bookProgress
        );
    }

    @Override
    public String toString() {
        return "BookDetailsDto{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", authors=" + authors +
                ", publicationYear=" + publicationYear +
                ", isbn='" + isbn + '\'' +
                ", nbPages=" + nbPages +
                ", lang='" + lang + '\'' +
                ", edition='" + edition + '\'' +
                ", summary='" + summary + '\'' +
                ", category='" + category + '\'' +
                ", bookProgress='" + bookProgress + '\'' +
                '}';
    }
}
