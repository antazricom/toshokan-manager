package com.antazri.toshokan.manager.api.security.auth.service;

@FunctionalInterface
public interface AuthenticationService {

    String auth(String login, String password);
}
