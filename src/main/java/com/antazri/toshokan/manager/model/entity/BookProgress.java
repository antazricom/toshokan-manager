package com.antazri.toshokan.manager.model.entity;

import com.antazri.toshokan.manager.model.utils.BookProgressStatus;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "book_progress")
public class BookProgress extends AbstractEntity {

    @Id
    @SequenceGenerator(name = "book_progress_id_seq", sequenceName = "book_progress_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_progress_id_seq")
    private Integer id;

    @Column(name = "nb_read_pages", nullable = false)
    private Integer nbReadPages;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private BookProgressStatus status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    public BookProgress() {
    }


    public BookProgress(Integer nbReadPages, BookProgressStatus bookProgressStatus, Book book) {
        this.nbReadPages = nbReadPages;
        this.status = bookProgressStatus;
        this.book = book;
    }

    public BookProgress(Integer id, Integer nbReadPages, BookProgressStatus bookProgressStatus, Book book) {
        this.id = id;
        this.nbReadPages = nbReadPages;
        this.status = bookProgressStatus;
        this.book = book;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNbReadPages() {
        return nbReadPages;
    }

    public void setNbReadPages(Integer nbReadPages) {
        this.nbReadPages = nbReadPages;
    }

    public BookProgressStatus getStatus() {
        return status;
    }

    public void setStatus(BookProgressStatus bookProgressStatus) {
        this.status = bookProgressStatus;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookProgress that = (BookProgress) o;
        return Objects.equals(id, that.id)
                && Objects.equals(nbReadPages, that.nbReadPages)
                && status == that.status
                && Objects.equals(book, that.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nbReadPages, status, book);
    }
}
