package com.antazri.toshokan.manager.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "book")
public class Book extends Item {

    @NotBlank
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "book_author",
            joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id")
    )
    private List<Author> authors;

    @Column(name = "publication_year")
    private Integer publicationYear;

    @Column(name = "isbn", unique = true)
    private String isbn;

    @Min(1)
    @Column(name = "nb_pages", nullable = false)
    private Integer nbPages;

    @Enumerated(EnumType.STRING)
    @Column(name = "lang", nullable = false)
    private Lang lang;

    @Column(name = "edition", nullable = false)
    private String edition;

    @Column(name = "summary", length = 10000)
    private String summary;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    @OneToOne(mappedBy = "book", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
    private BookProgress bookProgress;

    public Book() {
    }

    public Book(
            String title,
            List<Author> authors,
            Integer publicationYear,
            String isbn,
            Integer nbPages,
            Lang lang,
            String edition,
            String summary,
            Category category
    ) {
        this.title = title;
        this.authors = authors;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.nbPages = nbPages;
        this.lang = lang;
        this.edition = edition;
        this.summary = summary;
        this.category = category;
    }

    public Book(
            Integer id,
            String title,
            List<Author> authors,
            Integer publicationYear,
            String isbn,
            Integer nbPages,
            Lang lang,
            String edition,
            String summary,
            Category category
    ) {
        super(id);
        this.title = title;
        this.authors = authors;
        this.publicationYear = publicationYear;
        this.isbn = isbn;
        this.nbPages = nbPages;
        this.lang = lang;
        this.edition = edition;
        this.summary = summary;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(Integer publicationYear) {
        this.publicationYear = publicationYear;
    }

    public Integer getNbPages() {
        return nbPages;
    }

    public void setNbPages(Integer nbPages) {
        this.nbPages = nbPages;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Lang getLang() {
        return lang;
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public BookProgress getBookProgress() {
        return bookProgress;
    }

    public void setBookProgress(BookProgress bookProgress) {
        this.bookProgress = bookProgress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title)
                && Objects.equals(authors, book.authors)
                && Objects.equals(publicationYear, book.publicationYear)
                && Objects.equals(isbn, book.isbn)
                && Objects.equals(nbPages, book.nbPages)
                && lang == book.lang
                && Objects.equals(edition, book.edition)
                && Objects.equals(summary, book.summary)
                && Objects.equals(category, book.category)
                && Objects.equals(bookProgress, book.bookProgress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                title,
                authors,
                publicationYear,
                isbn,
                nbPages,
                lang,
                edition,
                summary,
                category,
                bookProgress
        );
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", authors=" + authors +
                ", publicationYear=" + publicationYear +
                ", isbn='" + isbn + '\'' +
                ", nbPages=" + nbPages +
                ", lang=" + lang +
                ", edition='" + edition + '\'' +
                ", summary='" + summary + '\'' +
                ", category=" + category +
                ", bookProgress=" + bookProgress +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public enum Lang {
        FR("fr"),
        EN("en"),
        JP("jp");

        private final String name;

        Lang(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}