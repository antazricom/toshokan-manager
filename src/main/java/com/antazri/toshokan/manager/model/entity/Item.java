package com.antazri.toshokan.manager.model.entity;

import jakarta.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Item extends AbstractEntity {

    @Id
    @SequenceGenerator(name = "item_id_seq", sequenceName = "item_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_id_seq")
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "item")
    private Record record;

    public Item() {
    }

    public Item(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Record getExpense() {
        return record;
    }

    public void setExpense(Record record) {
        this.record = record;
    }
}
