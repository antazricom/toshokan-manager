package com.antazri.toshokan.manager.model.entity;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "record")
public class Record {

    @Id
    @SequenceGenerator(name = "record_id_seq", sequenceName = "record_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "record_id_seq")
    private Integer id;

    @Column(name = "date")
    private LocalDateTime date;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private Item item;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(name = "number")
    private Integer number;

    @Column(name = "location")
    private String location;

    @Column(name = "expense")
    private double expense;

    @Column(name = "income")
    private double income;

    public Record(LocalDateTime date, Item item, Type type, Integer number, String location, double expense, double income) {
        this.date = date;
        this.item = item;
        this.type = type;
        this.number = number;
        this.location = location;
        this.expense = expense;
        this.income = income;
    }

    public Record(Integer id, LocalDateTime date, Item item, Type type, Integer number, String location, double expense, double income) {
        this(date, item, type, number, location, expense, income);
        this.id = id;
    }

    public Record() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getExpense() {
        return expense;
    }

    public void setExpense(double expense) {
        this.expense = expense;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return Double.compare(expense, record.expense) == 0
                && Double.compare(income, record.income) == 0
                && Objects.equals(id, record.id)
                && Objects.equals(date, record.date)
                && Objects.equals(item, record.item)
                && type == record.type
                && Objects.equals(number, record.number)
                && Objects.equals(location, record.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, item, type, number, location, expense, income);
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", date=" + date +
                ", item=" + item +
                ", type=" + type +
                ", number=" + number +
                ", location=" + location +
                ", expense=" + expense +
                ", income=" + income +
                '}';
    }

    public enum Type {
        BOOK, VINYL;
    }
}
