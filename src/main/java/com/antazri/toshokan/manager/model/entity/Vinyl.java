package com.antazri.toshokan.manager.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "vinyl")
@NamedQueries({
        @NamedQuery(name = "Vinyl.FindAll", query = "SELECT v FROM Vinyl v"),
        @NamedQuery(name = "Vinyl.FindByArtist", query = "SELECT v FROM Vinyl v WHERE v.artist.id = :id"),
        @NamedQuery(name = "Vinyl.FindByCategory", query = "SELECT v FROM Vinyl v WHERE v.category.id = :id"),
        @NamedQuery(name = "Vinyl.FindByPublicationYear", query = "SELECT v FROM Vinyl v WHERE v.publicationYear = :year"),
        @NamedQuery(name = "Vinyl.FindByTitle", query = "SELECT v FROM Vinyl v WHERE lower(v.title) LIKE lower(:title) "),
        @NamedQuery(name = "Vinyl.FindByDateRange", query = "SELECT v FROM Vinyl v WHERE v.publicationYear BETWEEN :start AND :end")
})
public class Vinyl extends Item {

    @NotBlank
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "artist_id", referencedColumnName = "id")
    private Artist artist;

    @Column(name = "publication_year")
    private Integer publicationYear;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    public Vinyl() {
    }

    public Vinyl(String title, Artist artist, Integer publicationYear, Category category) {
        this.title = title;
        this.artist = artist;
        this.publicationYear = publicationYear;
        this.category = category;
    }

    public Vinyl(Integer id, String title, Artist artist, Integer publicationYear, Category category) {
        super(id);

        this.title = title;
        this.artist = artist;
        this.publicationYear = publicationYear;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(Integer publicationYear) {
        this.publicationYear = publicationYear;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vinyl vinyl = (Vinyl) o;
        return Objects.equals(title, vinyl.title)
                && Objects.equals(artist, vinyl.artist)
                && Objects.equals(publicationYear, vinyl.publicationYear)
                && Objects.equals(category, vinyl.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, artist, publicationYear, category);
    }

    @Override
    public String toString() {
        return "Vinyl{" +
                "title='" + title + '\'' +
                ", artist=" + artist +
                ", publicationYear=" + publicationYear +
                ", category=" + category +
                '}';
    }
}
