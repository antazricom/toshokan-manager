package com.antazri.toshokan.manager.model.utils;

public enum BookProgressStatus {

    NEW ("NEW"),
    READING("READING"),
    COMPLETE ("COMPLETE"),
    PAUSED ("PAUSED"),
    SOLD ("SOLD"),
    AVAILABLE ("ABAILABLE");

    private String name;

    BookProgressStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
